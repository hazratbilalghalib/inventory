<?php
use Intervention\Image\Facades\Image;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::post('login', ['as' => 'login', 'uses' => 'Authentication\AuthenticationController@login']);
Route::get('/', function () {
    return redirect("login");
});
Route::group([
    'prefix' => 'password',
], function () {
    Route::post('reset-request', 'Authentication\AuthenticationController@resetRequest');
    Route::get('find/{token}', 'Authentication\AuthenticationController@find');
    Route::post('reset', 'Authentication\AuthenticationController@reset');
});

$router->group(['middleware' => 'auth'], function () {
    Route::get('home', function () {
        return view('home/home');
    });

    Route::resource('consumer', 'Consumer\ConsumerController');
    Route::post('customer/search', 'Consumer\ConsumerController@search');

    Route::resource('discount', 'Discount\DiscountController');
    Route::post('discount/search', 'Discount\DiscountController@search');
    Route::get('consumer-sale-limit/{consumer_id}', 'Consumer\ConsumerController@saleLimitonsumer');
    Route::resource('consumer', 'Consumer\ConsumerController');
    Route::post('customer/search', 'Consumer\ConsumerController@search');
    Route::resource('item/category', 'Item\ItemCategoryController');
    Route::post('item/category/search', 'Item\ItemCategoryController@search');
    Route::resource('item', 'Item\ItemController');
    Route::get('item/search', 'Item\ItemController@search');
    Route::get('item-search', 'Item\ItemController@itemSearch');
    Route::get('item-search-list', 'Item\ItemController@itemSearchList');
    Route::resource('transaction', 'Transaction\TransactionMasterController');
    Route::post('transaction/search', 'Transaction\TransactionMasterController@search');
    Route::resource('demand', 'Demand\DemandMasterController');
    Route::post('demand/search', 'Demand\DemandMasterController@search');
    Route::get('demand-item-search', 'Demand\DemandMasterController@itemSearch');
    Route::get('demand-item-search-list', 'Demand\DemandMasterController@itemSearchList');
    Route::resource('permission', 'Permission\PermissionController');
    Route::resource('user', 'User\UserController');
    Route::post('user-search', 'User\UserController@search');
    Route::get('profile', 'User\UserController@profile');
    Route::post('change-profile-pic', 'User\UserController@updateProfilePic')->name('change-profile-pic');

});

Route::get('login', function () {
    return view('auth/login');
});
Route::get('logout', 'Authentication\AuthenticationController@logout');

Route::get('storage/app/public/profile/{filename}', function ($filename) {
    return Image::make(storage_path('app/public/profile/' . $filename))->response();
});

// policy Routes of consumer
// Route::get('consumer/view', 'ConsumerController@view');
// Route::get('consumer/create', 'ConsumerController@create');
// Route::get('consumer/update', 'ConsumerController@update');
// Route::get('consumer/delete', 'ConsumerController@delete');
