<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsumersTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('consumers', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->string('name');
   $table->string('type')->nullable();
   $table->decimal('order_limit')->default(0);
   $table->string('sales_representative')->nullable();
   $table->string('region')->nullable();
   $table->string('sales_tax_registration_no')->nullable();
   $table->string('person_to_contact')->nullable();
   $table->string('designation')->nullable();
   $table->string('country')->nullable();
   $table->string('province')->nullable();
   $table->string('city')->nullable();
   $table->string('address_one')->nullable();
   $table->string('address_two')->nullable();
   $table->string('zip')->nullable();
   $table->string('phone')->nullable();
   $table->string('fax')->nullable();
   $table->string('email')->nullable();
   $table->text('remarks')->nullable();
   $table->timestamps();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('consumers');
 }
}
