<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionDetailsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('transaction_details', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->bigInteger('transaction_master_id')->unsigned();
   $table->bigInteger('item_id')->unsigned();
   $table->decimal('total_amount');
   $table->integer('item_quantity');
   $table->decimal('item_rate');
   $table->float('item_discount_percentage')->nullable();
   $table->timestamps();

   $table->foreign('transaction_master_id')->references('id')->on('transaction_masters')->onDelete('cascade');
   $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
  
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('transaction_details');
 }
}
