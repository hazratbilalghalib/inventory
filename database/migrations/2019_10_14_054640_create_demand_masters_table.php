<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDemandMastersTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('demand_masters', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->bigInteger('consumer_id')->unsigned();
   $table->date('demand_date');
   $table->bigInteger('demand_no')->unsigned()->nullable();
   $table->enum('shift',['morning','evening','night']);
   $table->string('person_name')->nullable();
   $table->date('remarks')->nullable();
 

  
   $table->timestamps();

   $table->foreign('consumer_id')->references('id')->on('consumers')->onDelete('cascade');
  
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('demand_masters');
 }
}
