<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionMastersTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('transaction_masters', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->bigInteger('consumer_id')->unsigned();
   $table->enum('type',['sale','purchase']);
   $table->string('receipt_no');
   $table->date('transaction_date');
   $table->string('driver_name')->nullable();
   $table->string('van_no')->nullable();
   $table->string('supplier_ref_no')->nullable();
   $table->string('serial_no')->nullable();
   $table->longText('remark')->nullable();
   $table->enum('return_ind',['N','Y'])->nullable();
   $table->timestamps();

   $table->foreign('consumer_id')->references('id')->on('consumers')->onDelete('cascade');
  

   
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('transaction_masters');
 }
}
