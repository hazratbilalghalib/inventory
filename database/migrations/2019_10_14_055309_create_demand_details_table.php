<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDemandDetailsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('demand_details', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->bigInteger('demand_master_id')->unsigned();
   $table->bigInteger('item_id')->unsigned();
   $table->enum('demand_type',['routine','urgent']);
   $table->bigInteger('quantity')->unsigned();
   $table->timestamps();

   $table->foreign('demand_master_id')->references('id')->on('demand_masters')->onDelete('cascade');
   $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
  
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('demand_details');
 }
}
