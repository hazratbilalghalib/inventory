<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 { 
  Schema::create('items', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->bigInteger('item_category_id')->unsigned();
   $table->string('name');
   $table->integer('min_stock_level')->default(0);
   $table->integer('max_stock_level')->default(0);
   $table->integer('avg_purchase_rte');
   $table->integer('avg_sale_rte');
   $table->string('item_type');
   $table->string('brand_name')->nullable();
   $table->string('unit_abbrev');
   $table->string('size_title')->nullable();
   $table->string('color_name')->nullable();
   $table->string('quality_type')->nullable();
   $table->string('barcode_txt');
   $table->string('barcode_ind');
   $table->string('item_code');  
   $table->string('item_urdu')->nullable();
   $table->integer('margin_one')->nullable();
   $table->integer('margin_two')->nullable();
   $table->timestamps();
   $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('cascade');
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::drop('items');
 }
}
