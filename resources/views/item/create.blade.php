@extends('layouts.master')
@section('title', 'Create Item')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Items</h1>
        </div>
</div>
<form method="post" action="{{action('Item\ItemController@store')}}">
        <div class="row">
                <div class="col-md-10">
                        <div class="tile">
                                <h3 class="tile-title">Add Item Record</h3>
                                <div class="tile-body">
                                        @if($errors->all())
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Please fix below errors</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div>
                                        @endif
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="POST">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                      
                                      <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Item name</label>
                                                <input class="{{ $errors->first('name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Category's name" name="name" value="{{ old('name') }}" required>
                                                @if($errors->first('name'))
                                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Category Name</label>
                                                        <select name="item_category_id" class="form-control" value="{{ old('item_category_id') }}" class="{{ $errors->first('item_category_id') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Category Name</option>
                                                         @foreach($categories as $category)
                                                        <option value="{{ $category->id}}">{{ $category->name}}</option>
                                                        @endforeach
                                                        </select>
                                                @if($errors->first('item_category_id'))
                                                <div class="form-control-feedback">{{ $errors->first('item_category_id') }}
                                                </div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Min Stock Level</label>
                                                <input class="{{ $errors->first('min_stock_level') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Min Stock Level" name="min_stock_level" value="{{ old('min_stock_level') }}" pattern="[0-9]+([\.,][0-9]+)?">
                                                @if($errors->first('min_stock_level'))
                                                <div class="form-control-feedback">{{ $errors->first('min_stock_level') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Max Stock Level</label>
                                                <input class="{{ $errors->first('max_stock_level') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter max Stock Level" name="max_stock_level" value="{{ old('max_stock_level') }}" pattern="[0-9]+([\.,][0-9]+)?">
                                                @if($errors->first('max_stock_level'))
                                                <div class="form-control-feedback">{{ $errors->first('max_stock_level') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>   <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Average Purchase Rate</label>
                                                <input class="{{ $errors->first('avg_purchase_rte') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Average Purchase Rate" name="avg_purchase_rte" value="{{ old('avg_purchase_rte') }}" pattern="[0-9]+([\.,][0-9]+)?" required>
                                                @if($errors->first('avg_purchase_rte'))
                                                <div class="form-control-feedback">{{ $errors->first('avg_purchase_rte') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Average Sales Rate</label>
                                                <input class="{{ $errors->first('avg_sale_rte') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Average sale Rate" name="avg_sale_rte" value="{{ old('avg_sale_rte') }}" pattern="[0-9]+([\.,][0-9]+)?" required>
                                                @if($errors->first('avg_sale_rte'))
                                                <div class="form-control-feedback">{{ $errors->first('avg_sale_rte') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>   <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Brand name</label>
                                                <input class="{{ $errors->first('brand_name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Brand's name" name="brand_name" value="{{ old('brand_name') }}">
                                                @if($errors->first('brand_name'))
                                                <div class="form-control-feedback">{{ $errors->first('brand_name') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Item Type</label>
                                                        <select name="item_type" class="form-control" value="{{ old('item_type') }}" class="{{ $errors->first('item_type') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Item Type</option>
                                                        <option value="B">B</option>
                                                        <option value="S">S</option>
                                                        </select>
                                                @if($errors->first('item_type'))
                                                <div class="form-control-feedback">{{ $errors->first('item_type') }}
                                                </div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>
                                           <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Size Title</label>
                                                <input class="{{ $errors->first('size_title') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Size Title" name="size_title" value="{{ old('size_title') }}">
                                                @if($errors->first('size_title'))
                                                <div class="form-control-feedback">{{ $errors->first('size_title') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Unit Abbreviation</label>
                                                        <select name="unit_abbrev" class="form-control" value="{{ old('unit_abbrev') }}" class="{{ $errors->first('unit_abbrev') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Unit Abbreviation</option>
                                                        <option value="KG">KG</option>
                                                        <option value="POUND">POUND</option>
                                                        <option value="UNT">UNT</option>
                                                        </select>
                                                @if($errors->first('unit_abbrev'))
                                                <div class="form-control-feedback">{{ $errors->first('unit_abbrev') }}
                                                </div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>   <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Barcode  text</label>
                                                <input class="{{ $errors->first('barcode_txt') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Barcode text" name="barcode_txt" value="{{ old('barcode_txt') }}" required>
                                                @if($errors->first('barcode_txt'))
                                                <div class="form-control-feedback">{{ $errors->first('barcode_txt') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Barcode Indicator</label>
                                                        <select name="barcode_ind" class="form-control" value="{{ old('barcode_ind') }}" class="{{ $errors->first('barcode_ind') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Barcode Indicator</option>
                                                        <option value="S">S</option>
                                                        <option value="E">E</option>
                                                        </select>
                                                @if($errors->first('barcode_ind'))
                                                <div class="form-control-feedback">{{ $errors->first('barcode_ind') }}
                                                </div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>   <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="control-label">Item Code</label>
                                                <input class="{{ $errors->first('item_code') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Item Code" name="item_code" value="{{ old('item_code') }}" required>
                                                @if($errors->first('item_code'))
                                                <div class="form-control-feedback">{{ $errors->first('item_code') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Item Urdu</label>
                                                <input class="{{ $errors->first('item_urdu') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Item Urdu" name="item_urdu" value="{{ old('item_urdu') }}">
                                                @if($errors->first('item_urdu'))
                                                <div class="form-control-feedback">{{ $errors->first('item_urdu') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        </div>   
                                        <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Color Name</label>
                                                <input class="{{ $errors->first('color_name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Color's name" name="color_name" value="{{ old('color_name') }}">
                                                @if($errors->first('color_name'))
                                                <div class="form-control-feedback">{{ $errors->first('color_name') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Quality Type</label>
                                                <input class="{{ $errors->first('quality_type') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Quality Type" name="quality_type" value="{{ old('quality_type') }}">
                                                @if($errors->first('quality_type'))
                                                <div class="form-control-feedback">{{ $errors->first('quality_type') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                       
                                        </div>  
                                     
                                        <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Margin One</label>
                                                <input class="{{ $errors->first('margin_one') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Margin One" name="margin_one" value="{{ old('margin_one') }}">
                                                @if($errors->first('margin_one'))
                                                <div class="form-control-feedback">{{ $errors->first('margin_one') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                                <label>Margin Two</label>
                                                <input class="{{ $errors->first('margin_two') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Margin Two" name="margin_two" value="{{ old('margin_two') }}">
                                                @if($errors->first('margin_two'))
                                                <div class="form-control-feedback">{{ $errors->first('margin_two') }}</div>
                                                @endif
                                        </div>
                                        </div>
                                        
                                        </div>     
                                     
                                      
                                     
                                </div>
                                <div class="tile-footer">
                                        <button class="btn btn-primary" type="submit"><i
                                                        class="fa fa-fw fa-lg fa-check-circle"></i>Save Record</button>
                                </div>
                        </div>
                </div>
        </div>
</form>
@endsection
@section('scripts')
<script>
        // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
//     $('#company_id').select2();
//     $('#account_id').select2();
});
</script>

@endsection
