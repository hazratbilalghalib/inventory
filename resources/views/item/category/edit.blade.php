@extends('layouts.master')
@section('title', 'Edit Item Category')
@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-edit"></i> Item Category</h1>
  </div>
</div>
<form method="post" action="{{action('Item\ItemCategoryController@update', $id)}}">
  <div class="row">
    <div class="col-md-6">
      <div class="tile">
        <h3 class="tile-title">Edit Item Category Record</h3>
        <div class="tile-body">
          @if($errors->all())
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Please fix below errors</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
         
          @endif
          {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />
        
          <div class="form-group">
                                                <label class="control-label">Category name</label>
                                                <input class="{{ $errors->first('name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Category's name" name="name" value="{{ $itemCategory->name }}" required>
                                                @if($errors->first('name'))
                                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Indicator Type</label>
                                                        <select name="type_indicator" class="form-control"  value="{{ $itemCategory->type_indicator }}"  class="{{ $errors->first('type_indicator') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Indicator Type</option>
                                                        <option value="{{$itemCategory->type_indicator}}" selected>{{ ($itemCategory->type_indicator)== 'B' ? "Bakery" : "General"}}</option>
                                                        <option value="B" >Bakery</option>
                                                        <option value="G">General</option>
                                                        </select>
                                                @if($errors->first('type_indicator'))
                                                <div class="form-control-feedback">{{ $errors->first('type_indicator') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Discount Indicator</label>
                                                        <select name="discount_ind" class="form-control" value="{{ $itemCategory->discount_ind }}" class="{{ $errors->first('discount_ind') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Discount Indicator</option>
                                                        <option value="{{$itemCategory->discount_ind}}" selected>{{ ($itemCategory->discount_ind)== 'D' ? "Discount" : "None"}}</option>
                                                        <option value="D">Discount</option>
                                                        <option value="N">None</option>
                                                        </select>
                                                @if($errors->first('discount_ind'))
                                                <div class="form-control-feedback">{{ $errors->first('discount_ind') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Multi Print Indicator</label>
                                                        <select name="multi_print_ind" class="form-control" value="{{ $itemCategory->multi_print_ind }}" class="{{ $errors->first('multi_print_ind') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select print indicator</option>
                                                        <option value="{{$itemCategory->multi_print_ind}}" selected>{{ ($itemCategory->multi_print_ind)== 'Y' ? "Yes" : "No"}}</option>
                                                        <option value="Y">Yes</option>
                                                        <option value="N">No</option>
                                                        </select>
                                                @if($errors->first('multi_print_ind'))
                                                <div class="form-control-feedback">{{ $errors->first('multi_print_ind') }}
                                                </div>
                                                @endif
                                        </div>
        <div class="tile-footer">
          <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    // $('#company_id').select2();
    // $('#account_id').select2();
});
</script>
<script>
$(document).ready(function () {
    $('#company_id').on('change', function() {
               jQuery.ajax({
                  url: "{{ url('/api/accounts') }}",
                  method: 'post',
                  data: {
                     company_id: jQuery('#company_id').val()
                  },
                  success: function(result){
                          $('#account_id')
    .find('option')
    .remove()
    .end()
    .append('<option value="">Select an Account</option>')
    .val('');
                        for (var i = 0; i < result.length; ++i) {
                               $("#account_id").append($("<option />").val(result[i].id).text(result[i].description));
                                if(result[i].children_accounts.length){
                                       appendChilds(result[i].children_accounts);
                               }
                        }
                  }});
               });
});
</script>
<script>
     function appendChilds(result){
        for (var a = 0; a < result.length; ++a) {
                               $("#account_id").append($("<option />").val(result[a].id).text(
                                       function(){
                                               for(var j =0; j<result[a].level; j++){
                                                       spaces=spaces+'\xa0\xa0\xa0\xa0';
                                               }
                                                return spaces+''+result[a].code+'-'+result[a].description;
                                       }
                               ));
                               if(result[a].children_accounts.length){
                                       appendChilds(result[a].children_accounts);
                               }
                        }     
     }
</script>
@endsection
