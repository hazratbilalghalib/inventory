<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="DOCE WareHouse">
    <title>Doce - @yield('title')</title>
    <meta name="description" content="DOCE WareHouse by Acrologix Private Limited.">
    <meta name="keywords" content="Doce, DOCE WareHouse, Acrologix, Acrologix Pvt Ltd, Acro,DOCE WareHouse System">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Bilal Ghalib">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <!-- Font-icon css-->
   <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome/4.7.0/css/font-awesome.min.css')}}">
   <!-- Select2 css-->
   <link rel="stylesheet" type="text/css" href="{{asset('css/select2/4.0.10/css/select2.min.css')}}">
   <link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/croppie@2.6.4/croppie.css">
    <style>
    .form-control-feedback{
      color:#dc3545 !important;
    }
    .required:after{
      content:" *";
      font-weight:bold;
      color:red;
    }
  #image_profile  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
    </style>
    <!-- Page specific css-->
    @yield('stylesheets')
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    @include('partials.header')
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
   @include('partials.sidebar')
       <main class="app-content">
          @yield('content')
      </main>
    <!-- Essential javascripts for application to work-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
     <script src="{{asset('js/jQuery.print.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/sort-table.min.js')}}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{asset('js/plugins/pace.min.js')}}"></script>
    <!-- Select2 JS-->
    <script src="{{asset('js/select2/4.0.10/js/select2.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Page specific javascripts-->
    <script>
    var path = window.location.pathname;
    var anchor_tag = $(`a[href="${path}"]`);
    anchor_tag.addClass('active');
    anchor_tag.closest('li').closest('ul').closest('li').addClass('is-expanded');
    </script>
    <script>
    $('input.date:text').datepicker({
        dateFormat: 'dd-mm-yy'
    });
    </script>
    <script>
    $(document).ready(function () { $("input").attr("autocomplete", "off"); });
    </script>
    <script>
    $("input[required]").siblings("label").addClass("required");
    $("textarea[required]").siblings("label").addClass("required");
    $("select[required]").siblings("label").addClass("required");
    </script>
    <script>
    $("#account_id").on("change", function() {
    $(this).children().text(function(i, value) {
        if (this.selected) return $.trim(value);
        return $(this).data("def_value");
    });
}).children().each(function() {
    $(this).data("def_value", $(this).text());
});
    </script>
    <script>
    $('table').each(function() {
  $(this).addClass('js-sort-table')
})
    </script>
    @yield('scripts')
     @include('sweetalert::alert')
  </body>
</html>
