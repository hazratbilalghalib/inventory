<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="description"
    content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
  <title>FAS - @yield('title')</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
  <!-- Font-icon css-->
  <!-- Font-icon css-->
   <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome/4.7.0/css/font-awesome.min.css')}}">
  <!-- Page specific css-->
   @toastr_css
  @yield('stylesheets')
  <style>
    .form-control-feedback {
      color: #dc3545 !important;
    }
  </style>
</head>

<body>
  <section class="material-half-bg">
    <div class="cover"></div>
  </section>
  @yield('content')
  <!-- Essential javascripts for application to work-->
  <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('js/popper.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>
  <!-- The javascript plugin to display page loading on top-->
  <script src="{{asset('js/plugins/pace.min.js')}}"></script>
  <!-- Page specific javascripts-->
  @toastr_js
  @toastr_render
  @yield('scripts')
</body>

</html>