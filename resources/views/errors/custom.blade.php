@extends('layouts.master')
@section('title', 'Error')
@section('content')
<div class="page-error tile">
        <h1><i class="fa fa-exclamation-circle"></i>
                {{ $error }}
        </h1>
      </div>
@endsection