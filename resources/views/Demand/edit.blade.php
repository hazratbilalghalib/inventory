@extends('layouts.master')
@section('title', 'Edit Demand')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Demands </h1>
        </div>
</div>
<div class="container">
    <div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Find Item</h5>

                </div>

                <div class="modal-body">
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Search.." name="search" id="search">
                    </div>
                    <div class="card-body table-responsive p-0" id="listTable" style="height: 300px; overflow-y:auto;">

                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Item Code</th>
                                    <th>Item Name</th>
                                 

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <fieldset class="form-fieldset">
                        <form class="form-horizontal" method="post" action="{{action('Demand\DemandMasterController@update', $id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Dispatcher</label>
                                              <select name="consumer_id" class="form-control" value="{{ old('consumer_id') }}" class="{{ $errors->first('consumer_id') != '' ? 'form-control is-invalid' : 'form-control' }}" id="consumer_id" autofocus required>
                                                        
                                                        
                                                        <option value="" disabled>Please Select Dispatcher</option>
                                              <option value="{{$demandMaster->consumer->id}}" selected>{{$demandMaster->consumer->name }}</option>
                                                        @foreach($dispatchers as $dispatcher)
                                                        <option value="{{$dispatcher->id}}">{{$dispatcher->name}}</option>
                                                        @endforeach
    
                                                       
                                                        </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Demand# </label>
                                                <input type="text" name="demand_no" value="{{$demandMaster->demand_no}}" readOnly class="form-control form-control-sm" id="demandNo" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Date</label>
                                            <input type="date" name="demand_date" value="{{$demandMaster->demand_date}}"  class="form-control form-control-sm"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Shift</label>
                                                <select name="shift" class="form-control" value="{{ old('shift') }}" class="{{ $errors->first('shift') != '' ? 'form-control is-invalid' : 'form-control' }}" id="shift" required>
                                                        
                                                        
                                                    <option value="" disabled>Please Select Shift</option>
                                                    <option  value="{{$demandMaster->shift}}">{{ucwords($demandMaster->shift)}}</option>
                                                    <option value="morning">Morning</option>
                                                    <option value="evening">Evening</option>
                                                    <option value="night">Night</option>
                                                

                                                   
                                                    </select>
                                            </div>
                                        </div>
                                     
                                       

                                    </div>
                                    <div class="row" id="dispatcher_detail">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Person Name </label>
                                                <input type="text" name="person_name" id="person_name" value="{{$demandMaster->person_name}}"  class="form-control form-control-sm">
                                            </div>
                                        </div>
                                       
                                        
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <input type="text" name="remarks" value="{{$demandMaster->remarks}}" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                     
                                    </div>
                                 
                                    <div class="card-body table-responsive xs p-0 tableFixHead">
                                        <table class="table table-bordered table-hover" id="myTable">

                                            <thead>
                                                <tr>
                                                    <th style="color:black;">Category</th>
                                                    <th style="color:black;">Item Code</th>
                                                    <th style="color:black;">Item Name</th>
                                                    <th style="color:black;">Demand Type</th>
                                                    <th style="color:black;">Quantity</th>
                                                   
                                                   
                                                  
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($demandDetails as $detail)
                                                   <tr>
                                                   <input type="hidden" name="id[]" value="{{$detail->id}}" class="form-control form-control-sm id">
                                                    <input type="hidden" name="item_id[]" value="{{$detail->item_id}}" class="form-control form-control-sm item_id">
                                                 
                                                    <td> <input  name="item_category[]" value="{{$detail->item->itemCategory->name}}" size="5"  class="form-control form-control-sm item_category"  readonly></td>
                                                    <td> <input id="color" name="item_code[]" value="{{$detail->item->item_code}}" size="4"  class="form-control form-control-sm item_code"></td>
                                                    <td> <input type="text" name="item_name[]" size="5" value="{{$detail->item->name}}" class="form-control form-control-sm item_name" readonly></td>
                                                    <td> <select name="demand_type[]" class="form-control form-control-sm  demand_type">
                                                        
                                                        
                                                        <option value="" disabled>Please Select Type</option>
                                                    <option value="{{$detail->demand_type}}" selected>{{ucwords($detail->demand_type)}}</option>
                                                        <option value="routine">Routine</option>
                                                        <option value="urgent">Urgent</option>
                                                       
    
                                                       
                                                        </select></td>
                                                    <td> <input type="text" value="{{$detail->quantity}}" name="item_quantity[]" size="2" class="form-control form-control-sm item_quantity"></td>
   
                                                   </tr>
                                                @endforeach

                                            </tbody>
                                        </table>


                                    </div>
                                  
                                            <div style="float:right; padding-top:15px;">
                                    <input type="submit" accesskey="m" value="Update Record" class="submit btn btn-primary">
                                  
                                </div>



                                </div>
                               


                        </form>
                    </fieldset>


                </div>
            </div>



        </div>



    </div>

</div>
</div>
</div>
@endsection
@section('scripts')
<script>
        // In your Javascript (external .js resource or <script> tag)
        var globalIndex;
    var appendFunction = null;
   
    $(document).ready(function() {
        function appendForm() {
            for (let i = 0; i < 1; i++) {
                $('#myTable').append(`<tr id="row${i}">
                    <input type="hidden" name="id[]" value="0" class="form-control form-control-sm id">
                                                    <input type="hidden" name="item_id[]" class="form-control form-control-sm item_id">
                                                 
                                                    <td> <input  name="item_category[]" size="5"  class="form-control form-control-sm item_category"  readonly></td>
                                                    <td> <input id="color" name="item_code[]" size="4"  class="form-control form-control-sm item_code"></td>
                                                    <td> <input type="text" name="item_name[]" size="5" class="form-control form-control-sm item_name" readonly></td>
                                                    <td> <select name="demand_type[]" class="form-control form-control-sm  demand_type">
                                                        
                                                        
                                                        <option value="" selected disabled>Please Select Type</option>
                                                      
                                                        <option value="routine">Routine</option>
                                                        <option value="urgent">Urgent</option>
                                                       
    
                                                       
                                                        </select></td>
                                                    <td> <input type="text" name="item_quantity[]" size="2" class="form-control form-control-sm item_quantity"></td>
                                                    
                                                </tr>`);
            }
        }
        appendForm();
        appendFunction = appendForm;
        $(window).keydown(function(event) {

            if (event.keyCode == 13 && event.target.type != "submit") {

                event.preventDefault();

            }
        });
    
        $(document).on('keydown', '.item_code', function(e) {
            var index = $('.item_code').index(this);
            let item_quantity = $('.item_quantity').eq(index - 1);
            for (let j = 0; j < item_quantity.length; j++) {
                var quantity = item_quantity[j].value;
            }
            if ((quantity == 0 || quantity == '') && index > 0) {
                $(this).val('');
                $(this).blur();
                swal("Quantity is required");

            } else {
                if (e.which === 13) {
                    e.preventDefault();

                    var value = $(this).val();
                  

                    if (value == '' && index > 0) {
                        $('.submit').focus();

                    } else {
                        var index = $('.item_code').index(this);
                      
                       

                        $('.item_category').eq(index)[0].value = '';
                        $('.item_quantity').eq(index)[0].value = '';
                        $('.item_id').eq(index)[0].value = '';
                        $('.item_name').eq(index)[0].value = '';
                      
                          

                            $.get("http://192.168.100.119:8000/demand-item-search?item_name="+value, function(data, status, xhr) {
                                var items = data.items;
                         
                                var length = items.length;
                               
                                if (length == 1) {
                                    for (let i = 0; i < length; i++) {
                                     
                                        let item_id = $('.item_id').eq(index);
                                        $('.item_id').eq(index)[0].value = items[i].id;
                                        $('.item_code').eq(index)[0].value = items[i].item_code;
                                        $('.item_name').eq(index)[0].value = items[i].name;
                                        $('.item_category').eq(index)[0].value = items[i].item_category.name;
                                        $('.demand_type').eq(index).focus();
                                      
                                        
                                        
                                    }
                                } else if (length > 1) {
                                    $('#listTable tbody tr').remove();
                                    $('#search').val('');
                                    globalIndex = index;
                                    let i = 0;
                                    for (; i < length; i++) {
                                      
                                     
                                        $("#listTable tbody").append(`<tr class="row${i}" onclick="javascript: return selectRow('${i}','${items[i].id}',
                                    '${items[i].item_code}', '${items[i].name}','${items[i].item_category.name}');">
                                    <td>${items[i].item_category.name}</td>
                                    <td>${items[i].item_code}</td>
                                    <td>${items[i].name}</td>
                                  
                                    </tr>`);
                                    if(i== 0)
                                    {
                                        $('#listModal').modal('show');
                                        $('.row0').addClass('highlight');
                                    }
                                    }
                                   
                                  

                                } else {
                                    swal("No Item Found");
                                    $('.item_code').eq(index).focus();

                                }


                            }).fail(function(xhr, status, error) {
                                // swal("Session End Please login");
                                // setTimeout(function() {
                                //     window.location.replace("/");
                                // }, 2000);

                            });
                        }
                    }
                }
            


        });
     
     
        $(document).on('keydown', '.item_quantity', function(e) {
            $('.item_quantity').eq(this).focus();



            var index = $('.item_quantity').index(this);


            var item_id = $('.item_id').eq(index)[0].value;
            var demand_type = $('.demand_type').eq(index)[0].value;
            if (item_id == '') {
                $(this).blur();
                swal("Item is required");
            }
            else
            if (demand_type == '') {
                $(this).blur();
                swal("Demand is required");
            } else {

                if (e.which === 13) {
                    e.preventDefault();


            
                    var quantity = $('.item_quantity').eq(index)[0].value;

                    if (item_id) {
                        if (Number(quantity)) {
                            if (parseInt(quantity) != 0) {


                            
                                
                       

                             
                                  
                                  
                          
                         
                                var nextIndex = $('.item_quantity').index(this) + 1;
                                var status = $('.item_code').eq(nextIndex)[0];
                                }
                                if (status != undefined) {
                                   
                                    $('.item_code').eq(nextIndex).focus();
                                
                               
                                } else {
                                    $('#myTable').append(`<tr id="row${nextIndex}">
                                        <input type="hidden" name="id[]" value="0" class="form-control form-control-sm id">
                                        <input type="hidden" name="item_id[]" class="form-control form-control-sm item_id">
                                                 
                                                 <td> <input  name="item_category[]" size="5"  class="form-control form-control-sm item_category"  readonly></td>
                                                 <td> <input id="color" name="item_code[]" size="4"  class="form-control form-control-sm item_code"></td>
                                                 <td> <input type="text" name="item_name[]" size="5" class="form-control form-control-sm item_name" readonly></td>
                                                 <td> <select name="demand_type[]" class="form-control form-control-sm  demand_type">
                                                     
                                                     
                                                     <option value="" selected disabled>Please Select Type</option>
                                                   
                                                     <option value="routine">Routine</option>
                                                     <option value="urgent">Urgent</option>
                                                    
 
                                                    
                                                     </select></td>
                                                 <td> <input type="text" name="item_quantity[]" size="2" class="form-control form-control-sm item_quantity"></td>

                                                </tr>`);
                                    $('.item_code').eq(nextIndex).focus();
                                }
                            } else {
                                $('.item_quantity').eq(index)[0].value = '';

                                swal("Quantity must be greater than zero");
                                $('.item_quantity').eq(index).focus();


                            }
                        } else {
                            $('.item_quantity').eq(index)[0].value = '';

                            swal("Quantity  must be integer");
                            $('.item_quantity').eq(index).focus();
                        }
                    }


                }
            
        });

       
    
    
        // $('form').on('submit', function() {
        //     let value = $('.quantity').eq(globalIndex)[0].value;
        //     console.log(value);
          
        //     if(value != '')
        //     {
        //         form_submit = true;
        //     }
        //     else
        //     {
        //         swal("Please Enter Last Item Quantity");
        //         form_submit = false;
        //     }
          
          


        //     if (form_submit) {
        //         return false;
        //     } else {
        //         return false;
        //     }

        // });
        $('#search').keydown(function(e) {
            let search_value = $('.item_code').eq(globalIndex)[0].value
            let value = $('#search').val();
            if (value == '' || value == '%') {
                value = search_value;
            }

            if (e.which === 13) {
              
                $.get("http://192.168.100.119:8000/demand-item-search-list?item_name=" + value, function(data, status,xhr) {
                    var items = data.items;
                  
                    var length = items.length;
                    $('#listTable tbody tr').remove();
                    let i = 0;
                    for (; i < length; i++) {

                        $("#listTable tbody").append(`<tr class="row${i}" onclick="javascript: return selectRow('${i}','${items[i].id}',
                                    '${items[i].item_code}', '${items[i].name}', '${items[i].item_category.name}');">

                                    <td>${items[i].item_category.name}</td>
                                    <td>${items[i].item_code}</td>
                                    <td>${items[i].name}</td>
                               
                                    </tr>`);
                                    if(i == 0)
                                    {
                                        $('.row0').addClass('highlight');
                                     $('#listModal').modal('show');
                                    }
                    }
                    

                }).fail(function(xhr, status, error) {
                    swal("SomeThing Went Wrong");
                    // setTimeout(function() {
                    //     window.location.replace("/");
                    // }, 2000);

                });
            }

        });
      
    });

    function clearForm() {
        $('#myTable tbody tr').remove();
      
      
     
       
        appendFunction();
    }

    function selectRow(i, item_id, item_code, item_name,item_category) {
        if ($('.row' + i).hasClass("highlight")) {
            let saleType = $('#sale_type').val();
            $('.item_id').eq(globalIndex)[0].value = item_id;
            $('.item_code').eq(globalIndex)[0].value = item_code;
            $('.item_name').eq(globalIndex)[0].value = item_name;
          

              
            $('.item_category').eq(globalIndex)[0].value = item_category;
            $('#listModal').modal('hide');
            $('.item_quantity').eq(globalIndex).focus();
       
           
        
        
        } else {
            $('#listTable tbody tr').removeClass('highlight');
            $('.row' + i).addClass('highlight');
        }

    }

</script>
<style>

    /* Fix table head */
    .tableFixHead {
        overflow-y: auto;
        height: 305px;
    }

    .tableFixHead th {
        position: sticky;
        top: 0;
    }

    /* Just common table stuff. */
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        padding: 8px 16px;
    }

    th {
        background: #eee;
    }

    tbody {
        height: 100px;
    }


    /* thead th:last-child {
        /* width: 156px; */
    /* 140px + 16px scrollbar width */
    /* } */
    td,
    .text-wrap table td {
        padding: 0rem !important;
        border-top: 0px !important;
    }

    .mt-md-5,
    .my-md-5 {
        margin-top: 0rem !important;
    }

    .container {
        max-width: 100vw !important;
    }

    .col-12 {
        margin: 0px;
        padding: 0px;
    }

    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        margin: 0;
        padding: 0.5rem 0.5rem !important;
        position: relative;
    }

    #listTable tbody tr {
        cursor: pointer;
    }

    #listTable tbody tr.highlight td {
        background-color: blue;
        color: white;
    }
    .show_display
    {
        display:none;
    }
</style>

@endsection
