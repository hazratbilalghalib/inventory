@extends('layouts.master')
@section('title', 'Demand')
@section('search')
<li class="app-search">
  <form action="{{action('Demand\DemandMasterController@search')}}" method="POST">
    <input type="hidden" value="{{csrf_token()}}" name="_token" />
    <input class="app-search__input" name="search" type="search" placeholder="Search" value="{{ $search ?? '' }}">
    <button class="app-search__button" type="submit"><i class="fa fa-search"></i></button>
  </form>
</li>
@endsection
@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i>Demand</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                @if(session('message'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>{{ session('message') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div style="float:right; padding: 10px;">
                <a href="{{action('Demand\DemandMasterController@create')}}"
                                        class="btn btn-sm btn-primary">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                    </div>

                <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                            <th>Dispatcher Name</th>
                            <th>Person Name</th>
                            <th>Shift</th>
                            <th>Demand No.</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (count($demands) > 0)
                        @foreach ($demands as $demand)
                        <tr>
                            <td>{{ $demand->consumer->name }}</td>
                            <td>{{ $demand->person_name }}</td>
                            <td>{{$demand->shift}} </td>
                            <td>{{$demand->demand_no }}</td>
                            <td>{{ $demand->demand_date }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a href="{{action('Demand\DemandMasterController@edit',$demand->id)}}"
                                        class="btn btn-sm btn-primary">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                    <a href="{{ route('demand.destroy',$demand->id) }}"
                                        class="confirmation btn btn-sm btn-danger" data-title="Delete Record"
                                        data-text="Are you sure to delete this record and its associated data? ">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                         @else
                                                <tr>
                                                <td colspan="6" class="text-center">
                                                        No Record Found
                                                </td>
                                                </tr>
                                                @endif
                    </tbody>
                </table>
                {{ $demands->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- Page specific javascripts-->
<script>
    jQuery(document).on('click', '.confirmation', function (e) {
    e.preventDefault(); // Prevent the href from redirecting
    var linkURL = $(this).attr("href");
    var _title = $(this).attr("data-title");
    var _text = $(this).attr("data-text");
    warnBeforeRedirect(linkURL, _text, _title);
});

function warnBeforeRedirect(linkURL, _text, _title) {

    swal({
  title: _title,
  text: _text,
  type: "warning",
  icon: "warning",
  dangerMode: true,
  buttons: true,
}).then(function(isConfirm) {
  if(isConfirm){
 var form = $('<form>', {
            'method': 'POST',
            'action': linkURL
        });

        var hiddenInput = $('<input>', {
            'name': '_method',
            'type': 'hidden',
            'value': 'DELETE'
        });

        hiddenToken = $('<input>', {
            'name': '_token',
            'type': 'hidden',
            'value': jQuery('meta[name="csrf-token"]').attr('content')
        });

        form.append(hiddenInput).append(hiddenToken).appendTo('body').submit();
}else {
    swal("Cancelled", "Your record is safe :)", "info");
  }
});
}
</script>
@endsection
