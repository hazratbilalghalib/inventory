@extends('layouts.master')
@section('title', 'Create Consumer')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Consumer</h1>
        </div>
</div>
<form method="post" action="{{action('Consumer\ConsumerController@store')}}">
        <div class="row">
                <div class="col-md-6">
                        <div class="tile">
                                <h3 class="tile-title">Add Consumer Record</h3>
                                <div class="tile-body">
                                        @if($errors->all())
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Please fix below errors</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div>
                                        @endif
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="POST">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                      
                                      
                                        <div class="form-group">
                                                <label class="control-label">Consumer name</label>
                                                <input class="{{ $errors->first('name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Consumer's name" name="name" value="{{ old('name') }}" required>
                                                @if($errors->first('name'))
                                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Consumer Type</label>
                                                        <select id ="type" name="type" class="form-control" value="{{ old('type') }}" class="{{ $errors->first('type') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Consumer Type</option>
                                                        <option value="supplier">Supplier</option>
                                                        <option value="dispatcher">Dispatcher</option>
                                                        </select>
                                                @if($errors->first('type'))
                                                <div class="form-control-feedback">{{ $errors->first('type') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group" id="order_limit">
                                                <label class="control-label">Order Limit</label>
                                                <input class="{{ $errors->first('order_limit') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter order limit"
                                                        name="order_limit"  value="{{ old('order_limit') }}">
                                                @if($errors->first('order_limit'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('order_limit') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Sales Representative</label>
                                                <input class="{{ $errors->first('sales_representative') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Sales Representative"
                                                        name="sales_representative" value="{{ old('sales_representative') }}">
                                                @if($errors->first('sales_representative'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('sales_representative') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Region</label>
                                                <input class="{{ $errors->first('region') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Region" name="region" value="{{ old('region') }}">
                                                @if($errors->first('region'))
                                                <div class="form-control-feedback">{{ $errors->first('region') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Sales Tax Registration No.</label>
                                                <input class="{{ $errors->first('sales_tax_registration_no') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Sales Tax Registration No."
                                                        name="sales_tax_registration_no" value="{{ old('sales_tax_registration_no') }}">
                                                @if($errors->first('sales_tax_registration_no'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('sales_tax_registration_no') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Personal To Contact</label>
                                                <input class="{{ $errors->first('person_to_contact') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Personal To Contact"
                                                        name="person_to_contact" value="{{ old('person_to_contact') }}">
                                                @if($errors->first('person_to_contact'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('person_to_contact') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Designation</label>
                                                <input class="{{ $errors->first('designation') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Designation" name="designation" value="{{ old('designation') }}">
                                                @if($errors->first('designation'))
                                                <div class="form-control-feedback">{{ $errors->first('designation') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Country</label>
                                                <input class="{{ $errors->first('country') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Country" name="country">
                                                @if($errors->first('country'))
                                                <div class="form-control-feedback" value="{{ old('country') }}">{{ $errors->first('country') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Province</label>
                                                <input class="{{ $errors->first('province') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Province" name="province">
                                                @if($errors->first('province'))
                                                <div class="form-control-feedback" value="{{ old('province') }}">{{ $errors->first('province') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">City</label>
                                                <input class="{{ $errors->first('city') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter City" name="city" value="{{ old('city') }}">
                                                @if($errors->first('city'))
                                                <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Address One</label>
                                                <input class="{{ $errors->first('address_one') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Address One" name="address_one" value="{{ old('address_one') }}">
                                                @if($errors->first('address_one'))
                                                <div class="form-control-feedback">{{ $errors->first('address_one') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Address Two</label>
                                                <input class="{{ $errors->first('address_two') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Address Two" name="address_two" value="{{ old('address_two') }}">
                                                @if($errors->first('address_two'))
                                                <div class="form-control-feedback">{{ $errors->first('address_two') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Zip</label>
                                                <input class="{{ $errors->first('zip') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Zip" name="zip" value="{{ old('zip') }}">
                                                @if($errors->first('zip'))
                                                <div class="form-control-feedback">{{ $errors->first('zip') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Phone</label>
                                                <input class="{{ $errors->first('phone') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Phone" name="phone" value="{{ old('phone') }}" required>
                                                @if($errors->first('phone'))
                                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Email Address</label>
                                                <input class="{{ $errors->first('email') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Email Address" name="email" value="{{ old('email') }}">
                                                @if($errors->first('email'))
                                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Fax</label>
                                                <input class="{{ $errors->first('fax') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Fax" name="fax" value="{{ old('fax') }}">
                                                @if($errors->first('fax'))
                                                <div class="form-control-feedback">{{ $errors->first('fax') }}</div>
                                                @endif
                                        </div>
                                      
                                        <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <input class="{{ $errors->first('remarks') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Remarks" name="remarks" value="{{ old('remarks') }}">
                                                @if($errors->first('remarks'))
                                                <div class="form-control-feedback">{{ $errors->first('remarks') }}</div>
                                                @endif
                                        </div>
                                </div>
                                <div class="tile-footer">
                                        <button class="btn btn-primary" type="submit"><i
                                                        class="fa fa-fw fa-lg fa-check-circle"></i>Save Record</button>
                                </div>
                        </div>
                </div>
        </div>
</form>
@endsection
@section('scripts')
<script>
        // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
//     $('#company_id').select2();
//     $('#account_id').select2();
$("#type").change(function(){
var consumer = $("#type").val();
var a = "supplier";
var b = "dispatcher";
if(consumer == b){
$("#order_limit").hide();
}
else{
        if(consumer == a){
                $("#order_limit").show();
        }
}
});
});
</script>
<script>
$(document).ready(function () {
    $('#company_id').on('change', function() {
               jQuery.ajax({
                  url: "{{ url('/api/accounts') }}",
                  method: 'post',
                  data: {
                     company_id: jQuery('#company_id').val()
                  },
                  success: function(result){
                          $('#account_id')
    .find('option')
    .remove()
    .end()
    .append('<option value="">Select an Account</option>')
    .val('');
                        for (var i = 0; i < result.length; ++i) {
                               $("#account_id").append($("<option />").val(result[i].id).text(result[i].description));
                                if(result[i].children_accounts.length){
                                       appendChilds(result[i].children_accounts);
                               }
                        }
                  }});
               });
});
</script>
<script>
     function appendChilds(result){
        for (var a = 0; a < result.length; ++a) {
                                var spaces="";
                               $("#account_id").append($("<option />").val(result[a].id).text(
                                       function(){
                                               for(var j =0; j<result[a].level; j++){
                                                       spaces=spaces+'\xa0\xa0\xa0\xa0';
                                               }
                                                return spaces+''+result[a].code+'-'+result[a].description;
                                       }
                               ));
                               if(result[a].children_accounts.length){
                                       appendChilds(result[a].children_accounts);
                               }
                        }     
     }
</script>
@endsection
