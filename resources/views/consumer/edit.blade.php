@extends('layouts.master')
@section('title', 'Edit Consumer')
@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-edit"></i> Consumer</h1>
  </div>
</div>
<form method="post" action="{{action('Consumer\ConsumerController@update', $id)}}">
  <div class="row">
    <div class="col-md-6">
      <div class="tile">
        <h3 class="tile-title">Edit Consumer Record</h3>
        <div class="tile-body">
          @if($errors->all())
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Please fix below errors</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />


          <div class="form-group" >
            <label class="control-label">Customer name</label>
            <input class="{{ $errors->first('name') != '' ? 'form-control is-invalid' : 'form-control' }}" type="text"
              placeholder="Enter Account's name" name="name"  value="{{$consumer->name}}" required>
            @if($errors->first('name'))
            <div class="form-control-feedback">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="form-group">
                                                <label class="control-label">Consumer Type</label>
                                                        <select name="type" id ="type" class="form-control" value="{{ $consumer->type }}" class="{{ $errors->first('type') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                     @if($consumer->type)
                                                        <option value="{{ $consumer->type }}" selected>{{ $consumer->type }}
                                                          </option>
                                                             @endif
                                                        <option value="supplier">Supplier</option>
                                                        <option value="dispatcher">Dispatcher</option>
                                                        </select>
                                                @if($errors->first('type'))
                                                <div class="form-control-feedback">{{ $errors->first('type') }}
                                                </div>
                                                @endif
                                        </div>

<div class="form-group" id="order_limit">
                                                <label class="control-label">Order Limit</label>
                                                <input class="{{ $errors->first('order_limit') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter order limit"
                                                        name="order_limit"  value="{{$consumer->order_limit}}">
                                                @if($errors->first('order_limit'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('order_limit') }}</div>
                                                @endif
                                        </div>


                                        <div class="form-group">
                                                <label class="control-label">Sales Representative</label>
                                                <input class="{{ $errors->first('sales_representative') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Sales Representative"
                                                        name="sales_representative" value="{{ $consumer->sales_representative }}">
                                                @if($errors->first('sales_representative'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('sales_representative') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Region</label>
                                                <input class="{{ $errors->first('region') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Region" name="region" value="{{ $consumer->region }}">
                                                @if($errors->first('region'))
                                                <div class="form-control-feedback">{{ $errors->first('region') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Sales Tax Registration No.</label>
                                                <input class="{{ $errors->first('sales_tax_registration_no') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Sales Tax Registration No."
                                                        name="sales_tax_registration_no" value="{{ $consumer->sales_tax_registration_no }}">
                                                @if($errors->first('sales_tax_registration_no'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('sales_tax_registration_no') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Personal To Contact</label>
                                                <input class="{{ $errors->first('person_to_contact') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Personal To Contact"
                                                        name="person_to_contact" value="{{ $consumer->person_to_contact }}">
                                                @if($errors->first('person_to_contact'))
                                                <div class="form-control-feedback">
                                                        {{ $errors->first('person_to_contact') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Designation</label>
                                                <input class="{{ $errors->first('designation') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Designation" name="designation" value="{{ $consumer->designation }}">
                                                @if($errors->first('designation'))
                                                <div class="form-control-feedback">{{ $errors->first('designation') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Country</label>
                                                <input class="{{ $errors->first('country') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Country" name="country">
                                                @if($errors->first('country'))
                                                <div class="form-control-feedback" value="{{ $consumer->country }}">{{ $errors->first('country') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Province</label>
                                                <input class="{{ $errors->first('province') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Province" name="province" value="{{$consumer->province}}">
                                                @if($errors->first('province'))
                                                <div class="form-control-feedback" value="{{ old('province') }}">{{ $errors->first('province') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">City</label>
                                                <input class="{{ $errors->first('city') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter City" name="city" value="{{ $consumer->city }}">
                                                @if($errors->first('city'))
                                                <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Address One</label>
                                                <input class="{{ $errors->first('address_one') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Address One" name="address_one" value="{{ $consumer->address_one }}">
                                                @if($errors->first('address_one'))
                                                <div class="form-control-feedback">{{ $errors->first('address_one') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Address Two</label>
                                                <input class="{{ $errors->first('address_two') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Address Two" name="address_two" value="{{ $consumer->address_two }}">
                                                @if($errors->first('address_two'))
                                                <div class="form-control-feedback">{{ $errors->first('address_two') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Zip</label>
                                                <input class="{{ $errors->first('zip') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Zip" name="zip" value="{{ $consumer->zip }}">
                                                @if($errors->first('zip'))
                                                <div class="form-control-feedback">{{ $errors->first('zip') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Phone</label>
                                                <input class="{{ $errors->first('phone') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Phone" name="phone" value="{{ $consumer->phone }}" required>
                                                @if($errors->first('phone'))
                                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Email Address</label>
                                                <input class="{{ $errors->first('email') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Email Address" name="email" value="{{ old('email') }}">
                                                @if($errors->first('email'))
                                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Fax</label>
                                                <input class="{{ $errors->first('fax') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Fax" name="fax" value="{{ old('fax') }}">
                                                @if($errors->first('fax'))
                                                <div class="form-control-feedback">{{ $errors->first('fax') }}</div>
                                                @endif
                                        </div>

                                        <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <input class="{{ $errors->first('remarks') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Remarks" name="remarks" value="{{ old('remarks') }}">
                                                @if($errors->first('remarks'))
                                                <div class="form-control-feedback">{{ $errors->first('remarks') }}</div>
                                                @endif
                                        </div>
        <div class="tile-footer">
          <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
var consumer = $("#type").val();
var a = "supplier";
var b = "dispatcher";
if(consumer == b){
$("#order_limit").hide();
}
else{
        if(consumer == a){
                $("#order_limit").show();
        }
}
    // $('#company_id').select2();
    // $('#account_id').select2();
});
</script>
<script>
$(document).ready(function () {
    $('#company_id').on('change', function() {
               jQuery.ajax({
                  url: "{{ url('/api/accounts') }}",
                  method: 'post',
                  data: {
                     company_id: jQuery('#company_id').val()
                  },
                  success: function(result){
                          $('#account_id')
    .find('option')
    .remove()
    .end()
    .append('<option value="">Select an Account</option>')
    .val('');
                        for (var i = 0; i < result.length; ++i) {
                               $("#account_id").append($("<option />").val(result[i].id).text(result[i].description));
                                if(result[i].children_accounts.length){
                                       appendChilds(result[i].children_accounts);
                               }
                        }
                  }});
               });
});
</script>
<script>
     function appendChilds(result){
        for (var a = 0; a < result.length; ++a) {
                               $("#account_id").append($("<option />").val(result[a].id).text(
                                       function(){
                                               for(var j =0; j<result[a].level; j++){
                                                       spaces=spaces+'\xa0\xa0\xa0\xa0';
                                               }
                                                return spaces+''+result[a].code+'-'+result[a].description;
                                       }
                               ));
                               if(result[a].children_accounts.length){
                                       appendChilds(result[a].children_accounts);
                               }
                        }
     }
</script>
@endsection
