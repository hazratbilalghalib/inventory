@extends('layouts.master')
@section('title', 'Edit Transaction')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Sale/Purchase </h1>
        </div>
</div>
<div class="container">
    <div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Find Item</h5>

                </div>

                <div class="modal-body">
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Search.." name="search" id="search">
                    </div>
                    <div class="card-body table-responsive p-0" id="listTable" style="height: 300px; overflow-y:auto;">

                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Item Code</th>
                                    <th>Rate</th>
                                    <th>Barcode</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <fieldset class="form-fieldset">
                        <form class="form-horizontal" method="post" action="{{action('Transaction\TransactionMasterController@update',$id)}}">
                        {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Type(<small>Sale/Purchase</small>)</label>
                                              <select name="type" class="form-control" value="{{ old('type') }}" class="{{ $errors->first('type') != '' ? 'form-control is-invalid' : 'form-control' }}" id="sale_type" required>
                                                        
                                              <option value="{{$transactonMaster->type}}" selected>{{strtoupper($transactonMaster->type)}}</option>
                                              @if($transactonMaster->type == "sale")
                                              <option value="purchase">Purchase</option>
                                              @else
                                                     
                                                        
                                                        <option value="sale">Sale</option>
                                                        @endif
                                                        </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" id="receiptNo">Challan# </label>
                                                <input type="text" name="receipt_no" value="{{$transactonMaster->receipt_no}}" readOnly class="form-control form-control-sm" id="challanNo" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Date</label>
                                                <input type="date" name="transaction_date"  value="{{$transactonMaster->transaction_date}}" class="form-control form-control-sm" readOnly required>
                                            </div>
                                        </div>
                                     
                                        <div class="col-md-3">
                                            <div class="form-group" id="dispatcherName">
                                                <label class="control-label">Dispatcher Name</label>
                                              <select name="dispatcher_id" class="form-control" value="{{ old('dispatcher_id') }}" class="{{ $errors->first('dispatcher_id') != '' ? 'form-control is-invalid' : 'form-control' }}" id="dispatcher_id" required>
                                                        <option value="" disabled>Please Select Dispatcher Name</option>
                                                        @if($transactonMaster->type=="sale")
                                                        <option value="{{$transactonMaster->consumer_id}}" selected>{{$transactonMaster->consumer->name}}</option>
                                                        @endif
                                                        @if(count($dispatchers))
                                                        @foreach($dispatchers as $dispatcher)
                                                        <option value="{{$dispatcher->id}}">{{$dispatcher->name}}</option>
                                                        @endforeach
                                                        @endif
                                                    
                                                        </select>
                                            </div>
                                            <div class="form-group show_display" id="supplierName">
                                                <label class="control-label">Supplier Name</label>
                                              <select name="supplier_id" class="form-control" value="{{ old('supplier_id') }}" class="{{ $errors->first('supplier_id') != '' ? 'form-control is-invalid' : 'form-control' }}" id="supplier_id">
                                                        <option value="" disabled>Please Select Supplier Name</option>
                                                        @if($transactonMaster->type=="purchase")
                                                        <option value="{{$transactonMaster->consumer_id}}" selected>{{$transactonMaster->consumer->name}}</option>
                                                        @endif
                                                        @if(count($suppliers))
                                                        @foreach($suppliers as $supplier)
                                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                                        @endforeach
                                                        @endif
                                                        </select>
                                            </div>
                                        </div>
                                        

                                    </div>
                                    <div class="row" id="dispatcher_detail">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Driver Name </label>
                                                <input type="text" name="driver_name" id="driver_name"  value="{{$transactonMaster->driver_name}}" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Van #</label>
                                                <input type="text" name="van_no" id="van_no" value="{{$transactonMaster->van_no}}" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <input type="text" name="remark1" value="{{$transactonMaster->remark}}" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                     
                                    </div>
                                    <div class="row show_display" id="supplier_detail">
                                      
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Supplier Ref#</label>
                                                <input type="text" name="supplier_ref_no" value="{{$transactonMaster->supplier_ref_no}}" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Serial No</label>
                                                <input type="text" name="serial_no" value="{{$transactonMaster->serial_no}}" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Remarks</label>
                                                <input type="text" name="remark2" value="{{$transactonMaster->remark}}"  class="form-control form-control-sm">
                                            </div>
                                        </div>
                                     
                                    </div>
                                    <div class="card-body table-responsive xs p-0 tableFixHead">
                                        <table class="table table-bordered table-hover" id="myTable">

                                            <thead>
                                                <tr>
                                                    <th style="color:black;">Code</th>
                                                    <th style="color:black;">Name</th>
                                                    <th style="color:black;">Rate</th>
                                                    <th style="color:black;">Quantity</th>
                                                    <th style="color:black;">Discount</th>
                                                    <th style="color:black;">Discount %</th>
                                                  
                                                    <th style="color:black;">Amount</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($transactionDetails as $transactionDetail)
                                            <tr>
                                            <input type="hidden" name="id[]" value="{{$transactionDetail->id}}" class="form-control form-control-sm id">
                                            <input type="hidden" name="item_id[]" value="{{$transactionDetail->item_id}}" class="form-control form-control-sm item_id">
                                                    <input type="hidden" name="item_discount_amount[]" value="{{$transactionDetail->item_discount_amount}}" class="form-control form-control-sm item_discount_amount">
                                                    <input type="hidden" name="total_discount_amount[]"  value="{{$transactionDetail->total_amount}}" class="form-control form-control-sm total_discount_amount">
                                
                                                    <td> <input id="color" name="item_code[]"  value="{{$transactionDetail->item->item_code}}" size="4"  class="form-control form-control-sm item_code" autofocus></td>
                                                    <td> <input type="text" name="item_name[]" value="{{$transactionDetail->item->name}}" class="form-control form-control-sm item_name" readonly></td>
                                                    <td> <input type="text" name="item_rate[]" value="{{$transactionDetail->item_rate}}" size="6" class="form-control form-control-sm item_rate">
                                                    <td> <input type="text" name="item_quantity[]" value="{{$transactionDetail->item_quantity}}" size="4" class="form-control form-control-sm item_quantity"></td>
                                                    <td> <input type="text" name="discount_amount[]" value="0" size="2" class="form-control form-control-sm discount" readonly></td>
                                                    <td> <input type="text" name="discount_percentage[]" value="{{$transactionDetail->item_discount_percentage}}" size="2" class="form-control form-control-sm discount_percentage" readonly></td>
                                                   
                                
                                                    <td> <input type="text" size="7" name="total_amt[]"  value="{{$transactionDetail->total_amt}}" class="form-control form-control-sm total_amount" readonly></td>
                                            </tr>
                                            @endforeach



                                            </tbody>
                                        </table>


                                    </div>
                                    <div class="row" style="padding:10px">
                                  <div class="col-md-4">
                                  <h6 class="control-label">Total Amount</h6>
                                        <input class="form-control form-control-sm  sum_total_amount mt-1" value="{{$sum_total_amount}}" name="sum_total_amount" type="text" readonly>
                                  </div>
                                  <div class="col-md-4" id="total_payable_amount">
                                  <h6 class="control-label">Total Payable Amount</h6>
                                        <input class="form-control form-control-sm  total_payable_amount mt-1" value="{{$total_discount_amount}}" name="total_payable_amount" type="text" readonly>
                                  </div>
                                   
                                       
                                         
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;Sales Return&nbsp;&nbsp;&nbsp;
                                                        @if($transactonMaster->return_ind == 'Y')
                                                        <input type="checkbox" name="return_ind" id="sale_return" class="form-check-input" value="Y" checked>
                                                        @else
                                                        <input type="checkbox" name="return_ind" id="sale_return" class="form-check-input" value="N">
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div style="float:right">
                                    <input type="submit" accesskey="m" value="Update Record" class="submit btn btn-primary">
                                    
                                </div>



                                </div>
                               


                        </form>
                    </fieldset>


                </div>
            </div>



        </div>



    </div>

</div>
</div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready( function () {
    function appendForm() {
            var saleType = $('#sale_type').val();
            if(saleType == "sale")
             { 
               
                $("#supplierName").addClass("show_display");
             $("#supplier_detail").addClass("show_display");
                $("#dispatcher_detail").removeClass("show_display");
                $("#total_payable_amount").removeClass("show_display");
             $("#dispatcherName").removeClass("show_display");
                $("#supplier_id").val('');
                $("#supplier_id").removeAttr('required');
                $("#dispatcher_id").prop('required',true);
                $("#van_no").prop('required',true);
                $("#driver_name").prop('required',true);
               
                $("#receiptNo").text("Challan#");
                $('#challanNo').attr("readonly", "readonly");
             }
             else if(saleType == "purchase"){
              



             $("#supplierName").removeClass("show_display");
             $("#supplier_detail").removeClass("show_display");
                 $("#dispatcher_detail").addClass("show_display");
                 $("#total_payable_amount").addClass("show_display");
                $("#dispatcherName").addClass("show_display");
                $("#dispatcher_id").val('');
                $("#dispatcher_id").removeAttr('required');
                $("#supplier_id").prop('required',true);
                $("#van_no").removeAttr('required');
                $("#driver_name").removeAttr('required');
               
                $("#receiptNo").text("GRN No");
                $("#challanNo").removeAttr("readonly");
             }
               
        }
        appendForm();
});
        // In your Javascript (external .js resource or <script> tag)
        var globalIndex;
    var appendFunction = null;
    var challanNo = null;
    $(document).ready(function() {
      
        $(window).keydown(function(event) {

            if (event.keyCode == 13 && event.target.type != "submit") {

                event.preventDefault();

            }
        });
        $(document).on('change', '#sale_type', function(e) {
             var saleType = $('#sale_type').val();
         
           
           
           
             if(saleType == "sale")
             { 
               
                $("#supplierName").addClass("show_display");
             $("#supplier_detail").addClass("show_display");
                $("#dispatcher_detail").removeClass("show_display");
                $("#total_payable_amount").removeClass("show_display");
             $("#dispatcherName").removeClass("show_display");
                $("#supplier_id").val('');
                $("#supplier_id").removeAttr('required');
                $("#dispatcher_id").prop('required',true);
                $("#van_no").prop('required',true);
                $("#driver_name").prop('required',true);
                $('#challanNo').val('');
                $('#challanNo').val(challanNo)
                $("#receiptNo").text("Challan#");
                $('#challanNo').attr("readonly", "readonly");
             }
             else if(saleType == "purchase"){
              



             $("#supplierName").removeClass("show_display");
             $("#supplier_detail").removeClass("show_display");
                 $("#dispatcher_detail").addClass("show_display");
                 $("#total_payable_amount").addClass("show_display");
                $("#dispatcherName").addClass("show_display");
                $("#dispatcher_id").val('');
                $("#dispatcher_id").removeAttr('required');
                $("#supplier_id").prop('required',true);
                $("#van_no").removeAttr('required');
                $("#driver_name").removeAttr('required');
                challanNo = $('#challanNo').val();
                $('#challanNo').val('');
                $("#receiptNo").text("GRN No");
                $("#challanNo").removeAttr("readonly");
             }
        });
        $(document).on('keydown', '.item_code', function(e) {
            var index = $('.item_code').index(this);
            let item_quantity = $('.item_quantity').eq(index - 1);
            for (let j = 0; j < item_quantity.length; j++) {
                var quantity = item_quantity[j].value;
            }
            if ((quantity == 0 || quantity == '') && index > 0) {
                $(this).val('');
                $(this).blur();
                swal("Quantity is required");

            } else {
                if (e.which === 13) {
                    e.preventDefault();

                    var value = $(this).val();

                    if (value == '' && index > 0) {
                        $('.submit').focus();

                    } else {
                        var index = $('.item_code').index(this);
                        let amount = $('.total_amount').eq(index)[0].value;
                        if (amount > 0) {
                            let sum_total_amount = $(".sum_total_amount").val();
                            sum_total_amount = parseFloat(sum_total_amount);
                            sum_total_amount -= parseFloat(amount);
                            $(".sum_total_amount").val(sum_total_amount);
                            let totalPayableAmount = $(".total_payable_amount").val();
                            totalPayableAmount = parseFloat(totalPayableAmount);
                            totalPayableAmount -= parseFloat(amount);
                            $(".total_payable_amount").val(totalPayableAmount);
                        }

                        $('.item_name').eq(index)[0].value = '';
                        $('.item_quantity').eq(index)[0].value = '';
                        $('.item_rate').eq(index)[0].value = '';
                        $('.total_amount').eq(index)[0].value = '';
                     
                            if (value != '') {
                                var Type = $('#sale_type').val();
                                if(Type == 'sale')
                               {
                               var consumer_id =  $("#dispatcher_id").val();
                               
                            }
                            else{
                                var consumer_id = $("#supplier_id").val();
                               
                                }

                            $.get("http://192.168.100.119:8000/item-search?item_name=" + value + "&consumer_id="+consumer_id, function(data, status, xhr) {
                                var items = data.items;

                                var length = items.length;
                                var sale_type = $('#sale_type').val();
                                if (length == 1) {
                                    for (let i = 0; i < length; i++) {
                                     
                                        let item_id = $('.item_id').eq(index);
                                        $('.item_id').eq(index)[0].value = items[i].id;
                                        $('.item_code').eq(index)[0].value = items[i].item_code;
                                        $('.item_name').eq(index)[0].value = items[i].name;
                                       
                                        if(sale_type == "sale")
                                        {
                                        $('.item_rate').eq(index)[0].value = items[i].avg_sale_rte;
                                        }
                                        else{
                                            $('.item_rate').eq(index)[0].value = "";
                                        }
                                    
                                        if(items[i].discount_percentage)
                                        {
                                        $('.discount_percentage').eq(index)[0].value = items[i].discount_percentage;
                                        }
                                        else{
                                            $('.discount_percentage').eq(index)[0].value = 0;
                                        }
                                        $('.item_discount_amount').eq(index)[0].value =items[i].avg_sale_rte * (1-(items[i].discount_percentage/100)).toFixed(2);
                                        $('.discount').eq(index)[0].value = 0;
                                        $('.total_amount').eq(index)[0].value = 0;
                                        if(sale_type == "sale")
                                        {
                                        $('.item_quantity').eq(index).focus();
                                        }
                                        else{
                                            $('.item_rate').eq(index).focus();
                                        }
                                    }
                                } else if (length > 1) {
                                    $('#listTable tbody tr').remove();
                                    $('#search').val('');
                                    globalIndex = index;
                                    let i = 0;
                                    for (; i < length; i++) {
                                      
                                     
                                        $("#listTable tbody").append(`<tr class="row${i}" onclick="javascript: return selectRow('${i}','${items[i].id}',
                                    '${items[i].item_code}', '${items[i].name}','${items[i].discount_percentage}', '${items[i].avg_sale_rte}');">
                                    <td>${items[i].name}</td>
                                    <td>${items[i].item_code}</td>
                                    <td>${items[i].avg_sale_rte}</td>
                                    <td>${items[i].barcode_txt}</td>
                                    </tr>`);
                                    if(i== 0)
                                    {
                                        $('#listModal').modal('show');
                                        $('.row0').addClass('highlight');
                                    }
                                    }
                                   
                                  

                                } else {
                                    swal("No Item Found");
                                    $('.item_code').eq(index).focus();

                                }


                            }).fail(function(xhr, status, error) {
                                // swal("Session End Please login");
                                // setTimeout(function() {
                                //     window.location.replace("/");
                                // }, 2000);

                            });
                        }
                    }
                }
            }


        });
        $(document).on('keydown', '.item_rate', function(e) {
            var index = $('.item_rate').index(this);
            if (e.which === 13) {
                    e.preventDefault();
                    var rate = $('.item_rate').eq(index)[0].value;
                
                    if(rate != "")
                    {
                        $('.item_quantity').eq(index).focus();
                    }
                    else{
                        swal("Please Enter ITEM rate")
                    }

                 
            }
        });
        $(document).on('keydown', '.item_quantity', function(e) {
            $('.item_quantity').eq(this).focus();



            var index = $('.item_quantity').index(this);


            var item_id = $('.item_id').eq(index)[0].value;
            if (item_id == '') {
                $(this).blur();
                swal("Item is required");
            } else {

                if (e.which === 13) {
                    e.preventDefault();


                    var rate = $('.item_rate').eq(index)[0].value;
                    var quantity = $('.item_quantity').eq(index)[0].value;

                    if (item_id) {
                        if (Number(quantity)) {
                            if (parseInt(quantity) != 0) {


                                let discount_amount = $('.item_discount_amount').eq(index)[0].value;
                            
                                let totalDiscount = discount_amount * quantity;
                               
                               
                                let totalAmount = rate * quantity;
                                let total_old_amount = $('.total_amount').eq(index)[0].value;
                                if(isNaN(total_old_amount))
                                {
                                    total_old_amount  = 0;
                                }
                                $('.total_amount').eq(index)[0].value = totalAmount;
                                let sum_total_amount = $(".sum_total_amount").val();
                               
                                if( isNaN(sum_total_amount))
                                {
                                     sum_total_amount  = 0;
                                }
                             
                                sum_total_amount = parseFloat(sum_total_amount);
                                sum_total_amount -= parseFloat(total_old_amount);
                                sum_total_amount += parseFloat(totalAmount);
                                $(".sum_total_amount").val(sum_total_amount);
                                let totalPayableAmount = $(".total_payable_amount").val();
                                if(isNaN(totalPayableAmount))
                                {
                                    totalPayableAmount  = 0;
                                }
                                let total_old_discount= $('.total_discount_amount').eq(index)[0].value;
                                if(isNaN(total_old_discount))
                                {
                                    total_old_discount  = 0;
                                }
                             
                                totalPayableAmount = parseFloat(totalPayableAmount);
                             
                                totalPayableAmount -= parseFloat(total_old_discount);
                          
                                $('.total_discount_amount').eq(index)[0].value = totalDiscount.toFixed(2);
                                totalPayableAmount += parseFloat(totalDiscount);
                                $(".total_payable_amount").val(totalPayableAmount.toFixed(2));
                               
                         
                                let nextIndex = $('.item_quantity').index(this) + 1;
                                let status = $('.item_code').eq(nextIndex)[0];
                                if (status != undefined) {
                                    let item_rate = $('.item_rate').eq(index)[0].value;
                                    if(item_rate != '')
                                    {
                                    $('.item_code').eq(nextIndex).focus();
                                    }
                                    else{
                                        swal("Please Enter Item Rate");
                                    }
                                } else {
                                    $('#myTable').append(`<tr id="row${nextIndex}">
                                    <input type="hidden" name="id[]" value="0" class="form-control form-control-sm id">
                                                    <input type="hidden" name="item_id[]" class="form-control form-control-sm item_id">
                                                    <input type="hidden" name="item_discount_amount[]" value="0" class="form-control form-control-sm item_discount_amount">
                                                    <input type="hidden" name="total_discount_amount[]" value="0" class="form-control form-control-sm total_discount_amount">
                                                    <td> <input id="color" name="item_code[]" size="4" class="form-control form-control-sm item_code" autofocus>
                                                       </td>
                                                    <td> <input type="text" name="item_name[]" class="form-control form-control-sm item_name" readonly></td>
                                                    <td> <input type="text" name="item_rate[]" size="6" class="form-control form-control-sm item_rate" >
                                                    <td> <input type="text" name="item_quantity[]" size="4" class="form-control form-control-sm item_quantity"></td>
                                                    <td> <input type="text" name="discount_amount[]" size="6" class="form-control form-control-sm discount" readonly></td>
                                                    <td> <input type="text" name="discount_percentage[]" size="4" class="form-control form-control-sm discount_percentage" readonly></td>
                                
                                                  
                                                    <td> <input type="text" size="7" name="total_amt[]" class="form-control form-control-sm total_amount" readonly></td>

                                                </tr>`);
                                    $('.item_code').eq(nextIndex).focus();
                                }
                            } else {
                                $('.item_quantity').eq(index)[0].value = '';

                                swal("Quantity must be greater than zero");
                                $('.item_quantity').eq(index).focus();


                            }
                        } else {
                            $('.item_quantity').eq(index)[0].value = '';

                            swal("Quantity  must be integer");
                            $('.item_quantity').eq(index).focus();
                        }
                    }


                }
            }
        });

        $(document).on('focus', '.item_rate', function(e)
        {
           
           let index =  $('.item_rate').index(this)
        let value = $('.item_id').eq(index)[0].value;
            let saleType = $('#sale_type').val();
         
            if(saleType == "sale")
            {
               
                $('.item_rate').blur();
                $('.item_quantity').eq(index).focus();
            }
            else if(saleType == "purchase" && value !="")
            {
                $('.item_rate').eq(index).focus();
             return true;
            }
            else{
                swal("Please Select Type Or Item");
                $('.item_rate').blur();
            }

        });
    
    
        $('form').on('submit', function() {
            let sumTotalAmount = $('.sum_total_amount').val();
           var amount = parseFloat(sumTotalAmount);
            
            var form_submit = false;
            if(amount>0)
            {
                form_submit = true;
            }
            else
            {
                swal("Please Enter Items");
                form_submit = false;
            }
          
          


            if (form_submit) {
                return true;
            } else {
                return false;
            }

        });
        $('#search').keydown(function(e) {
            let search_value = $('.item_code').eq(globalIndex)[0].value
            let value = $('#search').val();
            if (value == '' || value == '%') {
                value = search_value;
            }

            if (e.which === 13) {
                var Type = $('#sale_type').val();
                                if(Type == 'sale')
                               {
                               var consumer_id =  $("#dispatcher_id").val();
                               
                            }
                            else{
                                var consumer_id = $("#supplier_id").val('');
                               
                                }
                $.get("http://192.168.100.146:8000/item-search-list?item_name=" + value + "&consumer_id="+consumer_id, function(data, status) {
                    var items = data.items;
                    var length = items.length;
                    $('#listTable tbody tr').remove();
                    let i = 0;
                    for (; i < length; i++) {

                        $("#listTable tbody").append(`<tr class="row${i}" onclick="javascript: return selectRow('${i}','${items[i].id}',
                                    '${items[i].item_code}', '${items[i].name}', '${items[i].discount_percentage}','${items[i].avg_sale_rte}');">

                                    <td>${items[i].name}</td>
                                    <td>${items[i].item_code}</td>
                                    <td>${items[i].avg_sale_rte}</td>
                                    <td>${items[i].barcode_txt}</td>
                                    </tr>`);
                                    if(i == 0)
                                    {
                                        $('.row0').addClass('highlight');
                                     $('#listModal').modal('show');
                                    }
                    }
                    

                }).fail(function(xhr, status, error) {
                    swal("SomeThing Went Wrong");
                    // setTimeout(function() {
                    //     window.location.replace("/");
                    // }, 2000);

                });
            }

        });
        $('#sale_return').change(function() {
            if ($('#sale_return').val() == "N") {
                $('#sale_return').val("Y");
            } else {
                $('#sale_return').val("N");
            }
        });

    });

    function clearForm() {
        $('#myTable tbody tr').remove();
      
      
     
        $('.sum_total_amount').val('0');
        $('.total_payable_amount').val('0');
        
        appendFunction();
    }

    function selectRow(i, item_id, item_code, item_name,item_discount, item_rate) {
        if ($('.row' + i).hasClass("highlight")) {
            let saleType = $('#sale_type').val();
            $('.item_id').eq(globalIndex)[0].value = item_id;
            $('.item_code').eq(globalIndex)[0].value = item_code;
            $('.item_name').eq(globalIndex)[0].value = item_name;
            if(!empty(item_discount))
            {
            $('.discount_percentage').eq(globalIndex)[0].value = item_discount;
            }
            else{
                $('.discount_percentage').eq(globalIndex)[0].value = 0;
            }
            $('.item_discount_amount').eq(globalIndex)[0].value = item_rate * (1-(item_discount/100));
            $('.total_amount').eq(globalIndex)[0].value = 0;
            if(saleType == "sale")
            {

              
            $('.item_rate').eq(globalIndex)[0].value = item_rate;
            $('#listModal').modal('hide');
            $('.item_quantity').eq(globalIndex).focus();
       
            }
            else{
                $('.item_rate').eq(globalIndex)[0].value = "";
                $('#listModal').modal('hide');
            $('.item_rate').eq(globalIndex).focus();
            }
        
        
        } else {
            $('#listTable tbody tr').removeClass('highlight');
            $('.row' + i).addClass('highlight');
        }

    }

</script>
<style>

    /* Fix table head */
    .tableFixHead {
        overflow-y: auto;
        height: 305px;
    }

    .tableFixHead th {
        position: sticky;
        top: 0;
    }

    /* Just common table stuff. */
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        padding: 8px 16px;
    }

    th {
        background: #eee;
    }

    tbody {
        height: 100px;
    }


    /* thead th:last-child {
        /* width: 156px; */
    /* 140px + 16px scrollbar width */
    /* } */
    td,
    .text-wrap table td {
        padding: 0rem !important;
        border-top: 0px !important;
    }

    .mt-md-5,
    .my-md-5 {
        margin-top: 0rem !important;
    }

    .container {
        max-width: 100vw !important;
    }

    .col-12 {
        margin: 0px;
        padding: 0px;
    }

    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        margin: 0;
        padding: 0.5rem 0.5rem !important;
        position: relative;
    }

    #listTable tbody tr {
        cursor: pointer;
    }

    #listTable tbody tr.highlight td {
        background-color: blue;
        color: white;
    }
    .show_display
    {
        display:none;
    }
</style>

@endsection
