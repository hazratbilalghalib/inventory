@extends('layouts.auth-master')
@section('title', 'Login')
@section('stylesheets')
<style>
  .form-group {
    margin-bottom: 0.3rem !important;
  }
</style>
@endsection
@section('content')
<section class="login-content">
  <div class="logo">
    <h1>WareHouse</h1>
  </div>
  <div class="login-box" style="min-height:430px;">
    <form class="login-form" method="post" action="{{action('Authentication\AuthenticationController@login')}}">
      @csrf
      <div class="form-group">
        <label class="control-label">Email</label>
        <input class="{{ $errors->first('email') != '' ? 'form-control is-invalid' : 'form-control' }}" type="email"
          placeholder="Email" autofocus name="email" value="{{ old('email') }}" required>
        @if($errors->first('email'))
        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
        @endif
      </div>
      <div class="form-group">
        <label class="control-label">PASSWORD</label>
        <input class="{{ $errors->first('password') != '' ? 'form-control is-invalid' : 'form-control' }}"
          type="password" placeholder="Password" name="password" required>
        @if($errors->first('password'))
        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
        @endif
      </div>
      
      <div class="form-group">
        <div class="utility">
          <div class="animated-checkbox">
            <label>
              <input type="checkbox" name="remember"><span class="label-text">Stay Signed in</span>
            </label>
          </div>
          <!-- <p class="semibold-text mb-2"><a href="#" data-toggle="flip" class="forgot-password">Forgot Password ?</a></p> -->
        </div>
      </div>
      <div class="form-group btn-container">
        <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
      </div>
    </form>
    <!-- <form class="forget-form"  action="{{action('Authentication\AuthenticationController@resetRequest')}}" method="post">
      <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
      @csrf
      <div class="form-group">
        <label class="control-label">EMAIL</label>
        <input class="form-control" type="text" placeholder="Email" name="email">
      </div>
      <div class="form-group btn-container">
        <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
      </div>
      <div class="form-group mt-3">
        <p class="semibold-text mb-0"><a href="#" data-toggle="flip" class="back-to-login"><i class="fa fa-angle-left fa-fw"></i> Back to
            Login</a></p>
      </div>
    </form> -->
  </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
  // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
</script>
<script>
$(".forgot-password").click(function(){
  $(".login-box").css("min-height", "300px");
});
$(".back-to-login").click(function(){
  $(".login-box").css("min-height", "430px");
});
</script>
@endsection
