@extends('layouts.auth-master')
@section('title', 'Login')
@section('stylesheets')
<style>
  .form-group {
    margin-bottom: 0.3rem !important;
  }
</style>
@endsection
@section('content')
<section class="lockscreen-content">
      <div class="logo">
        <h1>Financial Accounting System</h1>
      </div>
      <div class="lock-box"><img class="rounded-circle user-image" src="{{ isset($user->image) ? '/storage/app/public/profile/'.$user->image : '/storage/app/public/profile/128.jpg' }}">
        <h4 class="text-center user-name">{{ $user->name }}</h4>
        <form class="unlock-form" action="{{action('Authentication\AuthenticationController@reset')}}" method="post">
         @csrf
         <input name="token" type="hidden" value="{{ $passwordReset->token }}">
         <input name="email" type="hidden" value="{{ $passwordReset->email }}">
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" placeholder="Password" autofocus="" name="password">
            @if($errors->first('password'))
        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
        @endif
          </div>
          <div class="form-group">
            <label class="control-label">Confirm Password</label>
            <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation">
            @if($errors->first('password_confirmation'))
        <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
        @endif
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-unlock fa-lg"></i>UNLOCK </button>
          </div>
        </form>
        <p><a href="#">Not {{ $user->name }} ? Contact us.</a></p>
      </div>
    </section>
@endsection
@section('scripts')
<script type="text/javascript">
  // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
</script>
@endsection
