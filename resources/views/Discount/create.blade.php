@extends('layouts.master')
@section('title', 'Create Discount')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Discount</h1>
        </div>
</div>
<form method="post" action="{{action('Discount\DiscountController@store')}}">
        <div class="row">
                <div class="col-md-6">
                        <div class="tile">
                                <h3 class="tile-title">Add Discount Record</h3>
                                <div class="tile-body">
                                        @if($errors->all())
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Please fix below errors</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div>
                                        @endif
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="POST">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token" />

                                        <div class="form-group">
                                                <label class="control-label">Dispatcher Name</label>
                                                        <select name="consumer_id" class="form-control" value="{{ old('consumer_id') }}" class="{{ $errors->first('consumer_id') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Name</option>
                                                         @foreach($consumers as $consumer)
                                                        <option value="{{ $consumer->id}}">{{ $consumer->name}}</option>
                                                        @endforeach
                                                        </select>
                                                @if($errors->first('consumer_id'))
                                                <div class="form-control-feedback">{{ $errors->first('consumer_id') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Category Name</label>
                                                        <select name="item_category_id" class="form-control" value="{{ old('item_category_id') }}" class="{{ $errors->first('item_category_id') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled selected>Please Select Category Name</option>
                                                         @foreach($item_categories as $category)
                                                        <option value="{{ $category->id}}">{{ $category->name}}</option>
                                                        @endforeach
                                                        </select>
                                                @if($errors->first('item_category_id'))
                                                <div class="form-control-feedback">{{ $errors->first('item_category_id') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label>Discount Percentage</label>
                                                <input class="{{ $errors->first('discount_percentage') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Discount Percentage" name="discount_percentage" value="{{ old('discount_percentage') }}">
                                                @if($errors->first('discount_percentage'))
                                                <div class="form-control-feedback">{{ $errors->first('discount_percentage') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <div class="animated-radio-button">
                                                        <label>
                                                                <input type="radio" name="is_active" value="Yes"
                                                                        checked="checked">
                                                                <span class="label-text">Active</span>
                                                        </label>
                                                        <label>
                                                                <input type="radio" name="is_active" value="No">
                                                                <span class="label-text">In Active</span>
                                                        </label>
                                                </div>
                                                @if($errors->first('is_active'))
                                                <div class="form-control-feedback">{{ $errors->first('is_active') }}
                                                </div>
                                                @endif
                                        </div>





                                <div class="tile-footer">
                                        <button class="btn btn-primary" type="submit"><i
                                                        class="fa fa-fw fa-lg fa-check-circle"></i>Save Record</button>
                                </div>
                        </div>
                </div>
        </div>
</form>
@endsection
@section('scripts')
<script>
        // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
//     $('#company_id').select2();
//     $('#account_id').select2();
});
</script>

@endsection
