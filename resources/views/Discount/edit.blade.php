@extends('layouts.master')
@section('title', 'Edit Discount')
@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-edit"></i> Discount</h1>
  </div>
</div>
<form method="post" action="{{action('Discount\DiscountController@update', $id)}}">
  <div class="row">
    <div class="col-md-6">
      <div class="tile">
        <h3 class="tile-title">Edit Discount Record</h3>
        <div class="tile-body">
          @if($errors->all())
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Please fix below errors</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <input type="hidden" value="{{csrf_token()}}" name="_token" />

                                        <div class="form-group">
                                                <label class="control-label">Dispatcher Name</label>
                                                        <select name="consumer_id" class="form-control" value="{{ old('consumer_id') }}" class="{{ $errors->first('consumer_id') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled >Please Select Name</option>
                                                        @if($discount->consumer_id)
              <option value="{{ $discount->consumer->id }}" selected>{{ $discount->consumer->name }}
               </option>
                @endif

                                                      
                                                         @foreach($consumers as $consumer)
                                                        <option value="{{ $consumer->id}}">{{ $consumer->name}}</option>
                                                        @endforeach
                                                        </select>
                                                @if($errors->first('consumer_id'))
                                                <div class="form-control-feedback">{{ $errors->first('consumer_id') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label">Category Name</label>
                                                        <select name="item_category_id" class="form-control" value="{{ old('item_category_id') }}" class="{{ $errors->first('item_category_id') != '' ? 'form-control is-invalid' : 'form-control' }}" required>
                                                        <option value="" disabled >Please Select Category Name</option>
                                                       @if($discount->item_category_id)
              <option value="{{ $discount->ItemCategory->id }}" selected>{{ $discount->itemCategory->name }}
@endif

                                                        
                                                         @foreach($item_categories as $category)
                                                        <option value="{{ $category->id}}">{{ $category->name}}</option>
                                                        @endforeach
                                                        </select>
                                                @if($errors->first('item_category_id'))
                                                <div class="form-control-feedback">{{ $errors->first('item_category_id') }}
                                                </div>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                                <label>Discount Percentage</label>
                                                <input class="{{ $errors->first('discount_percentage') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter Discount Percentage" name="discount_percentage" value="{{ $discount->discount_percentage }}">
                                                @if($errors->first('discount_percentage'))
                                                <div class="form-control-feedback">{{ $errors->first('discount_percentage') }}</div>
                                                @endif
                                        </div>
                                        <div class="form-group">
            <div class="animated-radio-button">
              <label>
                <input type="radio"  name="is_active" value="Yes"  {{ ($discount->is_active=="Yes") ? "checked" : "" }}>
                <span class="label-text">Active</span>
              </label>
              <label>
                <input type="radio" name="is_active" value="No" {{ ($discount->is_active=="No") ? "checked" : "" }}>
                <span class="label-text">In Active</span>
              </label>
            </div>
            @if($errors->first('is_active'))
            <div class="form-control-feedback">{{ $errors->first('is_active') }}</div>
            @endif
          </div>
        </div>


        <div class="tile-footer">
          <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    // $('#company_id').select2();
    // $('#account_id').select2();
});
</script>
<script>
$(document).ready(function () {
    $('#company_id').on('change', function() {
               jQuery.ajax({
                  url: "{{ url('/api/accounts') }}",
                  method: 'post',
                  data: {
                     company_id: jQuery('#company_id').val()
                  },
                  success: function(result){
                          $('#account_id')
    .find('option')
    .remove()
    .end()
    .append('<option value="">Select an Account</option>')
    .val('');
                        for (var i = 0; i < result.length; ++i) {
                               $("#account_id").append($("<option />").val(result[i].id).text(result[i].description));
                                if(result[i].children_accounts.length){
                                       appendChilds(result[i].children_accounts);
                               }
                        }
                  }});
               });
});
</script>
<script>
     function appendChilds(result){
        for (var a = 0; a < result.length; ++a) {
                               $("#account_id").append($("<option />").val(result[a].id).text(
                                       function(){
                                               for(var j =0; j<result[a].level; j++){
                                                       spaces=spaces+'\xa0\xa0\xa0\xa0';
                                               }
                                                return spaces+''+result[a].code+'-'+result[a].description;
                                       }
                               ));
                               if(result[a].children_accounts.length){
                                       appendChilds(result[a].children_accounts);
                               }
                        }
     }
</script>
@endsection
