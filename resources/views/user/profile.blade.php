@extends('layouts.master')
@section('title', 'Profile Settings')
@section('stylesheets')
@endsection
@section('content')
<div class="row user">
  <div class="col-md-12 mb-3">
    <div class="profile">
      <div class="info" id="image_profile">
        <img class="user-img" id="user-img" src="{{ isset(auth()->user()->image) ? 'storage/app/public/profile/'.auth()->user()->image : 'storage/app/public/profile/128.jpg' }}">
        <p class="img_wrapper">
          <label for="upload_image" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i> Change Profile Picture
          </label>
          <input id="upload_image" class="upload_image" type="file" accept="image/*"/>
        </p>
        <h4>{{ auth()->user()->name }}</h4>
      </div>
      <div class="cover-image"></div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="tile user-settings">
      <h4 class="line-head">Settings</h4>
      <form method="post" action="{{action('User\UserController@update', auth()->user()->id)}}">
        <div class="row mb-4">
          {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <div class="col-md-8">
            <label>Full Name</label>
            <input class="form-control" type="text" name="name" value="{{ auth()->user()->name }}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 mb-4">
            <label>Email</label>
            <input class="form-control" type="email" name="email" value="{{ auth()->user()->email }}" readonly>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-8 mb-4">
            <label>Password</label>
            <input class="form-control" type="password" name="password">
          </div>
          <div class="col-md-8 mb-4">
            <label>Confirm Password</label>
            <input class="form-control" type="password" name="confirm_password">
          </div>
        </div>
        <div class="row mb-10">
          <div class="col-md-12">
            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> UPDATE</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center">
            <div id="image_demo" style="margin-top:30px"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
       <button class="btn btn-success crop_image">Crop & Upload Image</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function(){

 $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'circle' //square
    },
    boundary:{
      width:300,
      height:300
    },
    enableResize: true,
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $("#user-img").attr("src",response);
      $(".app-sidebar__user-avatar").attr("src",response);
      $.ajax({
        url:"{{ route('change-profile-pic') }}",
        type: "POST",
        data:{"image": response, "_token": "{{ csrf_token() }}"},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
      });
    })
  });

});
</script>
@endsection
