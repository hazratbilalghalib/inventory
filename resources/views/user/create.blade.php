@extends('layouts.master')
@section('content')
<div class="app-title">
        <div>
                <h1><i class="fa fa-edit"></i> Permissions</h1>
        </div>
</div>
<form method="post" action="{{action('User\UserController@store')}}">
        <div class="row">
                <div class="col-md-6">
                        <div class="tile">
                                <h3 class="tile-title">Add Permission</h3>
                                <div class="tile-body">
                                        @if($errors->all())
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Please fix below errors</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                        </div>
                                        @endif
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="POST">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token" />

                                        <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <input class="{{ $errors->first('name') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter User Name"
                                                        name="name" value="{{ old('name') }}" required>
                                                @if($errors->first('name'))
                                                <div class="form-control-feedback">{{ $errors->first('name') }}
                                                </div>
                                                @endif
                                        </div>

                      <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="{{ $errors->first('email') != '' ? 'form-control is-invalid' : 'form-control' }}" type="email"
                                 placeholder="Email" autofocus name="email" value="{{ old('email') }}" required>
                              @if($errors->first('email'))
                                 <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                   @endif
                            </div>
                            </div>

                                        <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input class="{{ $errors->first('password') != '' ? 'form-control is-invalid' : 'form-control' }}"
                                                        type="text" placeholder="Enter User Password"
                                                        name="password" value="{{ old('password') }}" required>
                                                @if($errors->first('password'))
                                                <div class="form-control-feedback">{{ $errors->first('password') }}
                                                </div>
                                                @endif
                                        </div>

                                        <div class="form-group">
                                                <label class="control-label">Login Enable</label>
                                                <select class="js-example-basic-single js-states form-control" name="enable" id="enable"
                                                        >
                                                        <option value="" disabled>Select Login  </option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>

                                                </select>
                                                @if($errors->first('enable'))
                                                <div class="form-control-feedback">{{ $errors->first('enable') }}
                                                </div>
                                                @endif
                                        </div>


                                          <div class="form-group ">
                                                <input type="checkbox" name="select-all" id="select-all" />

                                                <label id="select" style="font-weight: bold;" class="control-label">Select All Permissions</label>
                                                <label  id="un-select" style="font-weight: bold;" class="control-label">Unselect All Permissions</label>



                                                     @foreach ($permissions as $permission)
                                              <ul class="list-unstyled ">
                                              <li>
                                                        <input  type="checkbox"  name="title[]" value="{{$permission->id}}" > <span class="checkmark"></span>{{ $permission->title  }}
                                                        </li>
                                                        </ul>
                                                        @endforeach
                                                @if($errors->first('title'))
                                                <div class="form-control-feedback">{{ $errors->first('title') }}
                                                </div>
                                                @endif
                                        </div>


                                </div>
                                <div class="tile-footer">
                                        <button class="btn btn-primary" type="submit"><i
                                                        class="fa fa-fw fa-lg fa-check-circle"></i> Save Record</button>
                                </div>
                        </div>
                </div>
        </div>
</form>




@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#un-select").hide();
});
$('#select-all').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
            $("#select").hide();
            $("#un-select").show();
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
            $("#un-select").hide();
            $("#select").show();


        });
    }
});
</script>
@endsection
