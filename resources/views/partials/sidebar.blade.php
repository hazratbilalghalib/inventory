<aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{ isset(auth()->user()->image) ? '/storage/app/public/profile/'.auth()->user()->image : '/storage/app/public/profile/128.jpg' }}" style="width:50px;height:50px;">
        <div>
          <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="/"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
       
        @can('view',App\User::class)
      
        <li><a class="treeview-item" href="/user" rel="noopener"><i class="fa fa-user"></i>User</a></li>
  @endcan
  @can('view',App\Consumer::class)
    
            <li><a class="treeview-item" href="/consumer" rel="noopener"><i class="icon fa fa-users"></i></i>Consumer</a></li>
    @endcan
    @can('view',App\Discount::class)

            <li><a class="treeview-item" href="/discount" rel="noopener"><i class="icon fa fa-users"></i></i>Discount</a></li>
            @endcan
         
        </li>
        @can('view',App\Item::class)
        
         <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Item</span><i class="treeview-indicator fa fa-angle-right"></i></a>
         @endcan

         <ul class="treeview-menu">
         @can('view',App\ItemCategory::class)

         <li><a class="treeview-item" href="/item/category"><i class="icon fa fa-circle-o"></i>Categories</a></li>
@endcan
@can('view',App\Item::class)

         <li><a class="treeview-item" href="/item" rel="noopener"><i class="icon fa fa-circle-o"></i> Items</a></li>
        @endcan  
        </ul>
        </li>
        @can('view',App\TransactionMaster::class)

        <li><a class="treeview-item" href="/transaction" rel="noopener"><i class="icon fa fa-money"></i></i>Transaction</a></li>
        @endcan
        @can('view',App\DemandMaster::class)
        <li><a class="treeview-item" href="/demand" rel="noopener"><i class="fa fa-reply"></i></i>Demand</a></li>
        @endcan
        <!-- <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Cost Center</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/cost-center/create"><i class="icon fa fa-circle-o"></i> Add Cost Center</a></li>
            <li><a class="treeview-item" href="/cost-center" rel="noopener"><i class="icon fa fa-circle-o"></i> Manage Cost Centers</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Financial Year</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/financial-year/create"><i class="icon fa fa-circle-o"></i> Add Financial Year</a></li>
            <li><a class="treeview-item" href="/financial-year" rel="noopener"><i class="icon fa fa-circle-o"></i> Manage Financial Years</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Opening Balance</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/account-opening-balance/create"><i class="icon fa fa-circle-o"></i> Add Opening Balance</a></li>
            <li><a class="treeview-item" href="/account-opening-balance" rel="noopener"><i class="icon fa fa-circle-o"></i> Manage Opening Balances</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Customer</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/customer/create"><i class="icon fa fa-circle-o"></i> Add Customer</a></li>
            <li><a class="treeview-item" href="/customer" rel="noopener"><i class="icon fa fa-circle-o"></i> Manage Customers</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Voucher</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/voucher-master/create"><i class="icon fa fa-circle-o"></i> Enter Voucher</a></li>
            <li><a class="treeview-item" href="/voucher-master" rel="noopener"><i class="icon fa fa-circle-o"></i> Manage Vouchers</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Trial Balance</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item active" href="/trial-balance/create"><i class="icon fa fa-circle-o"></i> Generate Trial Balance</a></li>
          </ul>
        </li>
         <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="/general-settings/create"><i class="icon fa fa-circle-o"></i> General Settings</a>
            </li>
          </ul>
        </li> -->
      </ul>
    </aside>