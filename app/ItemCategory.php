<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{

 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'item_categories';

 /**
  * The database primary key value.
  *
  * @var string
  */
 protected $primaryKey = 'id';

 /**
  * Attributes that should be mass-assignable.
  *
  * @var array
  */
 protected $fillable = ['name', 'type_indicator', 'discount_ind', 'multi_print_ind'];

 public function item()
 {
  return $this->hasMany('App\Item');
 }


}
