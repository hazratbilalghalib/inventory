<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{

    protected $fillable = ['consumer_id', 'item_category_id', 'discount_percentage', 'is_active'];
    public function consumer()
    {
        return $this->belongsTo('App\Consumer', 'consumer_id');
    }

    public function itemCategory()
    {
        return $this->belongsTo('App\ItemCategory', 'item_category_id');
    }

}
