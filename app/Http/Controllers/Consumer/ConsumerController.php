<?php

namespace App\Http\Controllers\Consumer;

use Auth;
use App\Consumer;
use App\TransactionMaster;
use App\TransactionDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class ConsumerController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index(Request $request)
 {
  $user= Auth::user();
  if($user->can('view',Consumer::class)){
  
  $consumers = Consumer::
 
   OrderBy('name')
   
   ->paginate(10);

  return view('consumer.index', ['consumers' => $consumers]);
  }
  else{
    return redirect('/');
  }
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
  $user = Auth::user();
       

  
  if($user->can('create',Consumer::class)) {
  return view('consumer/create');
    
} else {
return redirect('/');
}
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  *
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
   
  $messages = [
   'required' => 'This field is required.',
  ];

  Validator::make($request->all(), [
   'name'       => 'required',
   'order_limit'=> 'required|numeric|between:0,999999999.99',
   'type'        => 'required',
   'phone'       => 'required',
   'phone'      => 'unique:consumers',
  ], $messages)->validate();
 

  $consumer = Consumer::create($request->all());
 
  Alert::alert('Saved', 'Your Record is saved!', 'success');
  return redirect('consumer');
 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {
  $consumer = Consumer::findOrFail($id);

  return $consumer;
 }

 /**
  * Show the form for edit a resource.
  *
  * @return \Illuminate\Http\Response
  */

 public function edit($id)
 {

 $user= Auth::user();
 if($user->can('edit',Consumer::class)){
  $consumer =  Consumer::find($id);
  return view('consumer/edit', compact('consumer', 'id'));

 } 
 else{
return redirect('/');
}
}

 /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $id)
 {
$user = Auth::user();
if($user->can('update',Consumer::class)){
  $consumer = Consumer::find($id);

  $messages = [
   'required' => 'This field is required.',
  ];

  Validator::make($request->all(), [
    'name'       => 'required',
   'order_limit'=> 'required|decimal',

    'type'        => 'required',
    'phone'       => 'required',
   'phone'      => 'unique:consumers,phone,' . $id . ',id',
  ], $messages)->validate();

  $consumer = Consumer::findOrFail($id);
  $consumer->update($request->all());

  Alert::alert('Update', 'Your Record is Updated!', 'success');
  return redirect('consumer');
}
else{
return redirect('/');

}
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */
 public function destroy($id )
 {
  $user= Auth::user();
  if($user->can('delete',Consumer::class)){
  Consumer::destroy($id);

  Alert::alert('Delete', 'Your Record is deleted!', 'success');
  return redirect('consumer');
}
else{
return redirect('/');

}


 }

 /**
  * Search table(s) based on query parameter
  *
  * @param \Illuminate\Http\Request $request
  *
  * @return \Illuminate\Http\Response
  */
 public function search(Request $request)
 {
  $search         = \Request::get('search');
  

  if ($search) {
    $consumers = Consumer::
 
    OrderBy('name','ASC')
    ->where(function ($query) use ($search) {
     $query->where('name', 'LIKE', "%$search%")
      ->orWhere('type', 'LIKE', "%$search%")
      ->orWhere('sales_representative', 'LIKE', "%$search%")
      ->orWhere('region', 'LIKE', "%$search%")
      ->orWhere('sales_tax_registration_no', 'LIKE', "%$search%")
      ->orWhere('person_to_contact', 'LIKE', "%$search%")
      ->orWhere('designation', 'LIKE', "%$search%")
      ->orWhere('country', 'LIKE', "%$search%")
      ->orWhere('province', 'LIKE', "%$search%")
      ->orWhere('city', 'LIKE', "%$search%")
      ->orWhere('address_one', 'LIKE', "%$search%")
      ->orWhere('address_two', 'LIKE', "%$search%")
      ->orWhere('zip', 'LIKE', "%$search%")
      ->orWhere('phone', 'LIKE', "%$search%")
      ->orWhere('fax', 'LIKE', "%$search%")
      ->orWhere('email', 'LIKE', "%$search%")
      ->orWhere('remarks', 'LIKE', "%$search%");
    })->paginate(10);
  } else {
    $consumers = Consumer::
 
    OrderBy('name')
    
    ->paginate(10);
  }
  return view('consumer.index', ['consumers' =>   $consumers , 'search' => $search]);
 }

 public function saleLimitonsumer($id)
 {
  $sum_total_amount = 0;
  $date = date("Y-m-d");
 
  $consumer = Consumer::findOrFail($id);
  $order_limit = $consumer->order_limit;
  if(!empty($consumer))
  {

   $transactionMasters = TransactionMaster::with('consumer')->where(['consumer_id'=>$consumer->id, 'transaction_date' => $date])->get();
    
     foreach($transactionMasters as $transactionMaster)
     {
    $transactionDetails = TransactionDetail::with('item')->where('transaction_master_id', $transactionMaster->id)->get();
 
    foreach ($transactionDetails as $transactionDetail) {
       
        $sum_total_amount += $transactionDetail->total_amount;
    }
  }

 
  }
  return response()->json(['sum_total_amount' => $sum_total_amount, 'order_limit'=> $order_limit ],200);
 }
 
 public function pdf(){
  $data = [
    'title' => 'First PDF for Medium',
    'heading' => 'Hello from 99Points.info',
    'content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'        
      ];
  $options = new Options();
  $options->set('isJavascriptEnabled', true);
  
  $pdf = PDF::loadView('consumer.pdf', $data
      

  )->setPaper('a4', 'landscape');
  return $pdf->download(date("l jS \of F Y h:i:s A") . 'report.pdf');

 }

}
