<?php

namespace App\Http\Controllers\Item;

use Alert;
use App\Http\Controllers\Controller;
use App\ItemCategory;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ItemCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $user = Auth::user();
        if ($user->can('view', ItemCategory::class)) {

            $itemCategories = ItemCategory::

                OrderBy('name')

                ->paginate(10);

            return view('item.category.index', ['itemCategories' => $itemCategories]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', ItemCategory::class)) {

            return view('item/category/create');
        } else {
            return redirect('/');
        }
    }

/**
 * Store a newly created resource in storage.
 *
 * @param \Illuminate\Http\Request $request
 *
 * @return \Illuminate\Http\Response
 */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',

        ];

        Validator::make($request->all(), [
            'name' => 'required',
            'type_indicator' => 'required',
            'discount_ind' => 'required',
            'multi_print_ind' => 'required',
        ], $messages)->validate();

        $itemCategory = ItemCategory::create($request->all());

        Alert::alert('Saved', 'Your Record is saved!', 'success');
        return redirect('item/category');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemCategory = ItemCategory::findOrFail($id);

        return $itemCategory;
    }

    /**
     * Show the form for edit a resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $user = Auth::user();
        if ($user->can('edit', ItemCategory::class)) {

            $itemCategory = ItemCategory::find($id);
            return view('item/category/edit', compact('itemCategory', 'id'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->can('update', ItemCategory::class)) {
            $messages = [
                'required' => 'This field is required.',
            ];

            Validator::make($request->all(), [
                'name' => 'required',
                'type_indicator' => 'required',
                'discount_ind' => 'required',
                'multi_print_ind' => 'required',
            ], $messages)->validate();

            $itemCategory = ItemCategory::find($id);
            $itemCategory->update($request->all());

            Alert::alert('Update', 'Your Record is Updated!', 'success');
            return redirect('item/category');
        } else {
            return redirect('/');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->can('delete', ItemCategory::class)) {

            ItemCategory::destroy($id);
            Alert::alert('Delete', 'Your Record is deleted!', 'success');
            return redirect('item/category');
        } else {
            return redirect('/');
        }
    }

    /**
     * Search table(s) based on query parameter
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = \Request::get('search');
        if ($search) {
            $itemCategories = ItemCategory::

                OrderBy('name', 'ASC')
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', "%$search%");

                })->paginate(10);
        } else {
            $itemCategories = ItemCategory::

                OrderBy('name')

                ->paginate(10);
        }
        return view('item.category.index', ['itemCategories' => $itemCategories, 'search' => $search]);
    }
}
