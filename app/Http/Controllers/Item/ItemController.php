<?php

namespace App\Http\Controllers\Item;

use Alert;
use Auth;
use App\Item;
use App\Discount;
use App\Consumer;
use App\ItemCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index(Request $request)
 {
  $user= Auth::user();
  if($user->can('view',Item::class)){
  
  $items = Item::with('itemCategory')
 
  ->orderBy('name')
  
  ->paginate(10);

 return view('item.index', ['items' => $items]);
  }
  else{
    return redirect('/');
  }
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
   $user= Auth::user();
   if($user->can('create',Item::class)){
  $categories = ItemCategory::all();
  return view('item/create',  ['categories' => $categories]);
   }
   else{
return redirect('/');
   }
 }

/**
 * Store a newly created resource in storage.
 *
 * @param \Illuminate\Http\Request $request
 *
 * @return \Illuminate\Http\Response
 */
 public function store(Request $request)
 {
  $messages = [
    'required' => 'This field is required.',

   ];
 
   Validator::make($request->all(), [
    'name'       => 'required',
    'item_category_id' => 'required',
    'barcode_txt' => 'required',
    'item_code' => 'required',
    'avg_sale_rte' => 'required',
    'avg_purchase_rte' => 'required',
   ], $messages)->validate();
  
 
   $item = Item::create($request->all());
  
   Alert::alert('Saved', 'Your Record is saved!', 'success');
   return redirect('item');
  
 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {
  $item = Item::findOrFail($id);

  return  $item;
 }

 /**
  * Show the form for edit a resource.
  *
  * @return \Illuminate\Http\Response
  */

 public function edit($id)
 {
   $user=Auth::user();
   if($user->can('edit',Item::class)){
    $categories = ItemCategory::all();
  $item =  Item::find($id);
  return view('item/edit', compact('item','categories', 'id'));
 }
 else{
return redirect('/');
}
}

 /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */

 public function update(Request $request, $id)
 {
  $user=Auth::user();
  if($user->can('edit',Item::class)){
  

  $messages = [
   'required' => 'This field is required.',
  ];

  Validator::make($request->all(), [
    'name'       => 'required',
    'item_category_id' => 'required',
    'barcode_txt' => 'required',
    'item_code' => 'required',
    'avg_sale_rte' => 'required',
    'avg_purchase_rte' => 'required',
  ], $messages)->validate();

  $item = Item::find($id);
  $item->update($request->all());

  Alert::alert('Update', 'Your Record is Updated!', 'success');
  return redirect('item');
}
else{
  return redirect('/');
}
  
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  *
  * @return \Illuminate\Http\Response
  */
 public function destroy($id)
 {
  $user=Auth::user();
  if($user->can('edit',Item::class)){
  
  Item::destroy($id);
  Alert::alert('Delete', 'Your Record is deleted!', 'success');
  return redirect('item');
  }
  else{
    return redirect('/');
  }
 }

 /**
  * Search table(s) based on query parameter
  *
  * @param \Illuminate\Http\Request $request
  *
  * @return \Illuminate\Http\Response
  */
 public function search(Request $request)
 {
  $search = \Request::get('search');
  if ($search) {
   $items = Item::
   
    OrderBy('name', 'ASC')
    ->where(function ($query) use ($search) {
     $query->where('name', 'LIKE', "%$search%")
     ->orWhere('item_code', 'LIKE', "%$search%")
     ->orWhere('barcode_txt', 'LIKE', "%$search%")
     ->orWhere('avg_sale_rte', 'LIKE', "%$search%")
     ->orWhere('avg_purchase_rte', 'LIKE', "%$search%")
     ->orWhereHas('itemCategory', function($query) use ($search)
     {
        $query->where('name', 'LIKE', "%$search%");
     });
     
    })->paginate(10);
  } else {
    $items = Item::
 
    OrderBy('name')
    
    ->paginate(10);
  }  
  return view('item.index', ['items' => $items, 'search' => $search]);
 }

 public function itemSearch(Request $request)
 {
   
  $search = \Request::get('item_name');
    
 
  if ($search) {
  $consumer = Consumer::find(\Request::get('consumer_id'));
  
  if(!empty( $consumer))
  {
    $consumer_type = $consumer->type;
  }
  else{
    $consumer_type = null;
  }
   $items = Item::With('itemCategory')->
   
    
    where(function ($query) use ($search) {
     $query->where('name', "$search")
     ->orWhere('item_code', "$search")
     ->orWhere('barcode_txt', "$search")
     ->orWhereHas('itemCategory', function($query) use ($search)
     {
        $query->where('name', "$search");
     });
     
    })->limit(300)->get();
    
    if($items->count())
    {
    foreach($items as $item)
    {
     
        if($consumer_type == 'dispatcher')
        {
          $category_id = $item->itemCategory->id;
          $discount = Discount::where(['consumer_id'=> $consumer->id, 'item_category_id' =>   $category_id, 'is_active' => 'Yes'])->first();
          
          if(!empty( $discount))
          {
            $item->discount_percentage = $discount->discount_percentage;
          }
          else{
            $item->discount_percentage = 0;
          }
        }
        else{
          $item->discount_percentage = 0;
        }
    }
      $length =  $items->count();
      return response()->json(['items' => $items, 'length' => $length],200); 
    }
    else{
      $items = Item::With('itemCategory')->
   
    
      where(function ($query) use ($search) {
       $query->where('name',"LIKE", "%$search%")
       ->orWhere('item_code',"LIKE", "%$search%")
       ->orWhere('barcode_txt',"LIKE", "%$search%")
       ->orWhereHas('itemCategory', function($query) use ($search)
       {
          $query->where('name',"LIKE", "%$search%");





       });
       
      })->limit(300)->get();
      foreach($items as $item)
      {
        
        if($consumer_type == 'dispatcher')
        {
          $category_id = $item->itemCategory->id;
          $discount = Discount::where(['consumer_id'=> $consumer->id, 'item_category_id' =>   $category_id, 'is_active' => 'Yes'])->first();
          
          if(!empty( $discount))
          {
            $item->discount_percentage = $discount->discount_percentage;
          }
          else{
            $item->discount_percentage = 0;
          }
        }
        else{
          $item->discount_percentage = 0;
        }
      }
      $length = $items->count();
      return response()->json(['items' => $items, 'length' => $length],200); 
     
    
    }
    
  }

 }

 public function itemSearchList(Request $request)
 {
   
  $search = \Request::get('item_name');

 
  if ($search) {
  
    $consumer = Consumer::find(\Request::get('consumer_id'));
  
    if(!empty( $consumer))
    {
      $consumer_type = $consumer->type;
    }
    else{
      $consumer_type = null;
    }
      $items = Item::With('itemCategory')
   
    
      ->where(function ($query) use ($search) {
       $query->where('name',"LIKE", "%$search%")
       ->orWhere('item_code',"LIKE", "%$search%")
       ->orWhere('barcode_txt',"LIKE", "%$search%")
       ->orWhereHas('itemCategory', function($query) use ($search)
       {
          $query->where('name',"LIKE", "%$search%");





       });
       
      })->limit(300)->get();
      if($items->count())
      {
        foreach($items as $item)
        {
         
            if($consumer_type == 'dispatcher')
            {
              $category_id = $item->itemCategory->id;
              $discount = Discount::where(['consumer_id'=> $consumer->id, 'item_category_id' =>   $category_id, 'is_active' => 'Yes'])->first();
              
              if(!empty( $discount))
              {
                $item->discount_percentage = $discount->discount_percentage;
              }
              else{
                $item->discount_percentage = 0;
              }
            }
            else{
              $item->discount_percentage = 0;
            }
        }
      $length = $items->count();
      return response()->json(['items' => $items, 'length' => $length],200); 
      }
      else{
        $items = Item::With('itemCategory')->limit(300)->get();
        foreach($items as $item)
        {
         
            if($consumer_type == 'dispatcher')
            {
              $category_id = $item->itemCategory->id;
              $discount = Discount::where(['consumer_id'=> $consumer->id, 'item_category_id' =>   $category_id, 'is_active' => 'Yes'])->first();
              
              if(!empty( $discount))
              {
                $item->discount_percentage = $discount->discount_percentage;
              }
              else{
                $item->discount_percentage = 0;
              }
            }
            else{
              $item->discount_percentage = 0;
            }
        }
        $length = $items->count();
        return response()->json(['items' => $items, 'length' => $length],200); 
      }
     
    
    }
    
  

 }
}
