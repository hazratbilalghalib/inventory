<?php

namespace App\Http\Controllers\Discount;

use App\Consumer;
use App\Discount;
use App\Http\Controllers\Controller;
use App\ItemCategory;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->can('view', Discount::class)) {

            $discounts = Discount::with('consumer', 'itemCategory')->latest()->paginate(10);
            return view('Discount.index', compact('discounts'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user = Auth::user();
        if ($user->can('create', Discount::class)) {
            $consumers = Consumer::where('type', 'dispatcher')->get();

            $item_categories = ItemCategory::all();
            return view('Discount.create', compact('consumers', 'item_categories'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',

        ];

        Validator::make($request->all(), [
            'consumer_id' => 'required',
            'item_category_id' => 'required',
            'discount_percentage' => 'required|numeric|between:0,100',
            'is_active' => 'required',
        ], $messages)->validate();
        $discount = Discount::create($request->all());
        Alert::alert('Saved', 'Your Record is saved!', 'success');
        return redirect('discount');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Auth::user();
        if ($user->can('edit', Discount::class)) {
            $discount = Discount::find($id);
            $consumers = Consumer::where('type', 'dispatcher')->get();

            $item_categories = ItemCategory::all();

            return view('discount/edit', compact('discount', 'id', 'consumers', 'item_categories'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        if ($user->can('update', Discount::class)) {
            $messages = [
                'required' => 'This field is required.',

            ];

            Validator::make($request->all(), [
                'consumer_id' => 'required',
                'item_category_id' => 'required',
                'discount_percentage' => 'required|numeric|between:0,100',
                'is_active' => 'required',
            ], $messages)->validate();
            $discount = Discount::find($id);
            $discount->update($request->all());
            Alert::alert('Update', 'Your Record is Updated!', 'success');
            return redirect('discount');
        } else {
            return redirect('/');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = Auth::user();
        if ($user->can('delete', Discount::class)) {
            Discount::destroy($id);

            Alert::alert('Delete', 'Your Record is deleted!', 'success');
            return redirect('discount');
        } else {
            return redirect('/');

        }
    }
    public function search(Request $request)
    {
        $search = \Request::get('search');
        if ($search) {
            $discounts = Discount::with('consumer', 'itemCategory')->where(function ($query) use ($search) {
                $query->where('discount_percentage', 'LIKE', "%$search%")
                    ->orWhere('is_active', 'LIKE', "%$search%")
                    ->orWhereHas('consumer', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('itemCategory', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });

            })->latest()->paginate(10);
        } else {
            $discounts = Discount::latest()->paginate(10);

        }
        if ($discounts->count() == 0) {
            $discounts = Discount::latest()->paginate(10);

        }
        return view('Discount.index', ['discounts' => $discounts]);

    }
}
