<?php

namespace App\Http\Controllers\Transaction;

use App\Consumer;
use App\Http\Controllers\Controller;
use App\TransactionDetail;
use App\TransactionMaster;
use Auth;
use Illuminate\Http\Request;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;

class TransactionMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->can('view', TransactionMaster::class)) {
            $transactions = TransactionMaster::with('consumer')->orderBy('transaction_date', 'desc')->paginate(10);
            return view('transaction.index')->with(['transactions' => $transactions]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', TransactionMaster::class)) {

            $dispatchers = Consumer::where('type', 'dispatcher')->get();
            $suppliers = Consumer::where('type', 'supplier')->get();
            $lastChallanNo = TransactionMaster::where('type', 'sale')->orderBy('created_at', 'DESC')->first();
            if (!empty($lastChallanNo)) {
                $challan_no = $lastChallanNo->receipt_no;
                ++$challan_no;
            } else {
                $challan_no = 10;
            }
            return view('transaction.create')->with(['challanNo' => $challan_no, 'dispatchers' => $dispatchers, 'suppliers' => $suppliers]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_name = strtoUpper(\Auth::user()->name);
        $Item_Ids_Arr = array_filter($request->item_id, function ($value) {
            return !is_null($value);
        });

        $Item_Codes_Arr = array_filter($request->item_code, function ($value) {
            return !is_null($value);
        });

        $Item_Names_Arr = array_filter($request->item_name, function ($value) {
            return !is_null($value);
        });

        $Item_Rates_Arr = array_filter($request->item_rate, function ($value) {
            return !is_null($value);
        });

        $Each_Item_Total_Discount_Amount_Arr = array_filter($request->total_discount_amount, function ($value) {

            return !is_null($value) && $value > 0;
        });
        $Item_Quantities_Arr = array_filter($request->item_quantity, function ($value) {
            return !is_null($value);
        });

        $Discount_Percentages_Arr = array_filter($request->discount_percentage, function ($value) {
            return !is_null($value);
        });

        $Discounts_Arr = array_filter($request->discount_amount, function ($value) {
            return !is_null($value);
        });

        $Each_Item_Total_Amount_Arr = array_filter($request->total_amt, function ($value) {
            return !is_null($value) && $value > 0;
        });

        if ($request->type == "sale") {
            $consumer_id = $request->dispatcher_id;
            $remark = $request->remark1;
        } else {
            $consumer_id = $request->supplier_id;
            $remark = $request->remark2;
        }
        if (empty($request->return_ind)) {
            $request->return_ind = "N";
        }

        $TransactionMaster = TransactionMaster::create(
            [

                "transaction_date" => $request->transaction_date,
                "type" => $request->type,
                "consumer_id" => $consumer_id,
                "receipt_no" => $request->receipt_no,
                "remark" => $remark,
                "driver_name" => $request->driver_name,
                "van_no" => $request->van_no,
                "supplier_ref_no" => $request->supplier_ref_no,
                "serial_no" => $request->serial_no,
                "return_ind" => $request->return_ind,
            ]
        );

        $length = count($Each_Item_Total_Discount_Amount_Arr);
        for ($i = 0; $i < $length; $i++) {

            $TransactionDetail = TransactionDetail::create(
                [
                    "transaction_master_id" => $TransactionMaster->id,
                    "item_id" => $Item_Ids_Arr[$i],
                    "total_amount" => $Each_Item_Total_Discount_Amount_Arr[$i],
                    "item_quantity" => $Item_Quantities_Arr[$i],
                    "item_rate" => $Item_Rates_Arr[$i],
                    "item_discount_percentage" => $Discount_Percentages_Arr[$i],

                ]
            );

        }
        $date = date_create($request->transaction_date);
        if ($request->type == "sale") {
            try {
                // Enter the share name for your USB printer here
                $connector = new WindowsPrintConnector("citizen");
                /* Print a "Hello world" receipt" */
                $printer = new Printer($connector);
                for ($k = 0; $k < 2; $k++) {
                    $printer->setFont(Printer::FONT_B);
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
                    $printer->setEmphasis(true);
                    $printer->feed();
                    $printer->text("Delivery Challan\n");
                    $printer->selectPrintMode();
                    $printer->setEmphasis(false);
                    $printer->text("_____________________________________________\n");
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->setEmphasis(true);
                    $printer->text(str_pad($request->receipt_no, 0) . "\n");
                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                    $printer->text(str_pad(" ", 18) . '' . str_pad("Voucher Info ", 25) . "\n\n");
                    $printer->text(str_pad(" ", 18) . '' . str_pad("Bilal Ghalib ", 25) . "\n");

                    $printer->setEmphasis(false);
                    $printer->text(str_pad(" ", 12) . '' . str_pad("Date:" . "  " . date_format($date, "d-M-y"), 30) . "\n");
                    $printer->text(str_pad(" ", 17) . '' . str_pad("Operator:" . "  " . strtoUpper($user_name), 30) . "\n");
                    $printer->text(str_pad(" ", 12) . '' . str_pad("Driver:" . "  " . strtoUpper($request->driver_name), 30) . "\n");
                    $printer->text(str_pad(" ", 17) . '' . str_pad("Vehicle #:" . "  " . strtoUpper($request->van_no), 30) . "\n");
                    $printer->feed();
                    $printer->setEmphasis(true);
                    $printer->text(str_pad("Item", 20) . '' . str_pad("Rate", 10) . '' . str_pad("Qty", 9) . ' ' . "Amount" . "\n");
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->text("_____________________________________________\n");
                    $printer->setEmphasis(false);
                    $printer->setTextSize(1, 1);
                    for ($i = 0; $i < $length; $i++) {
                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                        $printer->text(str_pad(substr($Item_Names_Arr[$i], 0, 19), 20) . '' . str_pad($Item_Rates_Arr[$i], 10) . '' . str_pad($Item_Quantities_Arr[$i], 9) . '' . $Each_Item_Total_Amount_Arr[$i] . "\n");

                        if (!empty(substr($Item_Names_Arr[$i], 24))) {
                            $printer->text(str_pad(substr($Item_Names_Arr[$i], 19), 20) . "\n");
                        }
                        if ($i < $length - 1) {
                            $printer->setJustification(Printer::JUSTIFY_CENTER);
                            $printer->text("_____________________________________________\n");
                        }
                    }
                    $printer->setTextSize(1, 1);
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->text("_____________________________________________\n");
                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                    $printer->text(str_pad("Total Items: " . $length, 25) . '' . str_pad("Total Amount: " . $request->sum_total_amount, 23));

                    $printer->text(str_pad(" ", 10) . '' . str_pad("Total Payable Amount: " . round($request->total_payable_amount), 30) . "\n");
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->text("_____________________________________________\n");

                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                    $printer->setEmphasis(true);

                    $printer->setTextSize(1, 1);
                    $printer->text(str_pad("Web: www.doce.pk", 20) . '' . str_pad("Helpine: +92 311 111 3623", 26));

                    $printer->feed();
                    $printer->cut();

                    /* Close printer */
                    $printer->close();
                }
            } catch (Exception $e) {

                echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
            }
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->can('edit', TransactionMaster::class)) {

            $dispatchers = Consumer::where('type', 'dispatcher')->get();
            $suppliers = Consumer::where('type', 'supplier')->get();

            $transactionMaster = TransactionMaster::with('consumer')->where('id', $id)->first();

            $transactionDetails = TransactionDetail::with('item')->where('transaction_master_id', $transactionMaster->id)->get();
            $sum_total_amount = 0;
            $total_discount_amount = 0;
            foreach ($transactionDetails as $transactionDetail) {
                $transactionDetail->item_discount_amount = $transactionDetail->item_rate * (1 - ($transactionDetail->item_discount_percentage / 100));
                $transactionDetail->total_amt = $transactionDetail->item_rate * $transactionDetail->item_quantity;
                $sum_total_amount += $transactionDetail->total_amt;
                $total_discount_amount += $transactionDetail->total_amount;
            }
            return view('transaction.edit')->with(['transactonMaster' => $transactionMaster, 'transactionDetails' => $transactionDetails, 'dispatchers' => $dispatchers, 'suppliers' => $suppliers, 'sum_total_amount' => $sum_total_amount, 'total_discount_amount' => $total_discount_amount, 'id' => $id]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->can('update', TransactionMaster::class)) {

            $user_name = strtoUpper(\Auth::user()->name);
            $Item_Ids_Arr = array_filter($request->item_id, function ($value) {
                return !is_null($value);
            });
            $Ids_Arr = array_filter($request->id, function ($value) {
                return !is_null($value);
            });

            $Item_Codes_Arr = array_filter($request->item_code, function ($value) {
                return !is_null($value);
            });

            $Item_Names_Arr = array_filter($request->item_name, function ($value) {
                return !is_null($value);
            });

            $Item_Rates_Arr = array_filter($request->item_rate, function ($value) {
                return !is_null($value);
            });

            $Each_Item_Total_Discount_Amount_Arr = array_filter($request->total_discount_amount, function ($value) {

                return !is_null($value);
            });
            $Item_Quantities_Arr = array_filter($request->item_quantity, function ($value) {
                return !is_null($value);
            });

            $Discount_Percentages_Arr = array_filter($request->discount_percentage, function ($value) {
                return !is_null($value);
            });

            $Discounts_Arr = array_filter($request->discount_amount, function ($value) {
                return !is_null($value);
            });

            $Each_Item_Total_Amount_Arr = array_filter($request->total_amt, function ($value) {
                return !is_null($value);
            });

            if ($request->type == "sale") {
                $consumer_id = $request->dispatcher_id;
                $remark = $request->remark1;
            } else {
                $consumer_id = $request->supplier_id;
                $remark = $request->remark2;
            }
            if (empty($request->return_ind)) {
                $request->return_ind = "N";
            }

            $TransactionMaster = TransactionMaster::Where('id', $id)->update(
                [

                    "transaction_date" => $request->transaction_date,
                    "type" => $request->type,
                    "consumer_id" => $consumer_id,
                    "receipt_no" => $request->receipt_no,
                    "remark" => $request->remark,
                    "driver_name" => $request->driver_name,
                    "van_no" => $request->van_no,
                    "supplier_ref_no" => $request->supplier_ref_no,
                    "serial_no" => $request->serial_no,
                    "return_ind" => $request->return_ind,
                ]
            );

            $length = count($Item_Ids_Arr);
            for ($i = 0; $i < $length; $i++) {
                if ($Ids_Arr[$i] != 0) {
                    $TransactionDetail = TransactionDetail::where(["transaction_master_id" => $id, 'id' => $Ids_Arr[$i]])->update(
                        [

                            "item_id" => $Item_Ids_Arr[$i],
                            "total_amount" => $Each_Item_Total_Discount_Amount_Arr[$i],
                            "item_quantity" => $Item_Quantities_Arr[$i],
                            "item_rate" => $Item_Rates_Arr[$i],
                            "item_discount_percentage" => $Discount_Percentages_Arr[$i],

                        ]
                    );
                } else {
                    $TransactionDetail = TransactionDetail::create(
                        [
                            "transaction_master_id" => $id,
                            "item_id" => $Item_Ids_Arr[$i],
                            "total_amount" => $Each_Item_Total_Discount_Amount_Arr[$i],
                            "item_quantity" => $Item_Quantities_Arr[$i],
                            "item_rate" => $Item_Rates_Arr[$i],
                            "item_discount_percentage" => $Discount_Percentages_Arr[$i],

                        ]
                    );
                }
            }
            $date = date_create($request->transaction_date);
            if ($request->type == "sale") {
                try {
                    // Enter the share name for your USB printer here
                    $connector = new WindowsPrintConnector("citizen");
                    /* Print a "Hello world" receipt" */
                    $printer = new Printer($connector);
                    for ($k = 0; $k < 2; $k++) {
                        $printer->setFont(Printer::FONT_B);
                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
                        $printer->setEmphasis(true);
                        $printer->feed();
                        $printer->text("Delivery Challan\n");
                        $printer->selectPrintMode();
                        $printer->setEmphasis(false);
                        $printer->text("_____________________________________________\n");
                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                        $printer->setEmphasis(true);
                        $printer->text(str_pad($request->receipt_no, 0) . "\n");
                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                        $printer->text(str_pad(" ", 18) . '' . str_pad("Voucher Info ", 25) . "\n\n");
                        $printer->text(str_pad(" ", 18) . '' . str_pad("Bilal Ghalib ", 25) . "\n");

                        $printer->setEmphasis(false);
                        $printer->text(str_pad(" ", 12) . '' . str_pad("Date:" . "  " . date_format($date, "d-M-y"), 30) . "\n");
                        $printer->text(str_pad(" ", 17) . '' . str_pad("Operator:" . "  " . strtoUpper($user_name), 30) . "\n");
                        $printer->text(str_pad(" ", 12) . '' . str_pad("Driver:" . "  " . strtoUpper($request->driver_name), 30) . "\n");
                        $printer->text(str_pad(" ", 17) . '' . str_pad("Vehicle #:" . "  " . strtoUpper($request->van_no), 30) . "\n");
                        $printer->feed();
                        $printer->setEmphasis(true);
                        $printer->text(str_pad("Item", 20) . '' . str_pad("Rate", 10) . '' . str_pad("Qty", 9) . ' ' . "Amount" . "\n");
                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                        $printer->text("_____________________________________________\n");
                        $printer->setEmphasis(false);
                        $printer->setTextSize(1, 1);
                        for ($i = 0; $i < $length; $i++) {
                            $printer->setJustification(Printer::JUSTIFY_LEFT);
                            $printer->text(str_pad(substr($Item_Names_Arr[$i], 0, 19), 20) . '' . str_pad($Item_Rates_Arr[$i], 10) . '' . str_pad($Item_Quantities_Arr[$i], 9) . '' . $Each_Item_Total_Amount_Arr[$i] . "\n");

                            if (!empty(substr($Item_Names_Arr[$i], 24))) {
                                $printer->text(str_pad(substr($Item_Names_Arr[$i], 19), 20) . "\n");
                            }
                            if ($i < $length - 1) {
                                $printer->setJustification(Printer::JUSTIFY_CENTER);
                                $printer->text("_____________________________________________\n");
                            }
                        }
                        $printer->setTextSize(1, 1);
                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                        $printer->text("_____________________________________________\n");
                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                        $printer->text(str_pad("Total Items: " . $length, 25) . '' . str_pad("Total Amount: " . $request->sum_total_amount, 23));

                        $printer->text(str_pad(" ", 10) . '' . str_pad("Total Payable Amount: " . round($request->total_payable_amount), 30) . "\n");
                        $printer->setJustification(Printer::JUSTIFY_CENTER);
                        $printer->text("_____________________________________________\n");

                        $printer->setJustification(Printer::JUSTIFY_LEFT);
                        $printer->setEmphasis(true);

                        $printer->setTextSize(1, 1);
                        $printer->text(str_pad("Web: www.doce.pk", 20) . '' . str_pad("Helpine: +92 311 111 3623", 26));

                        $printer->feed();
                        $printer->cut();

                        /* Close printer */
                        $printer->close();
                    }
                } catch (Exception $e) {

                    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
                }
            }
            return redirect()->back();
        } else {
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->can('delete', TransactionMaster::class)) {

            TransactionMaster::destroy($id);
            \Alert::alert('Delete', 'Your Record is deleted!', 'success');
            return redirect('transaction');
        } else {
            return redirect('/');
        }
    }

    public function search()
    {
        $search = \Request::get('search');
        if ($search) {
            $transactions = TransactionMaster::with('consumer')->

                orderBy('transaction_date', 'desc')
                ->where(function ($query) use ($search) {
                    $query->where('type', 'LIKE', "%$search%")
                        ->orWhere('receipt_no', 'LIKE', "%$search%")
                        ->orWhere('transaction_date', 'LIKE', "%$search%")

                        ->orWhereHas('consumer', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%");
                        });

                })->paginate(10);
        } else {
            $transactions = TransactionMaster::with('consumer')->orderBy('transaction_date', 'desc')->paginate(10);

        }
        return view('transaction.index')->with(['transactions' => $transactions]);
    }
}
