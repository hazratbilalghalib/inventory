<?php

namespace App\Http\Controllers\User;

use Alert;
use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->can('view', User::class)) {

            $users = User::latest()->paginate(10);
            return view('user.index', compact('users'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', User::class)) {
            $permissions = Permission::all();
            return view('user.create', compact('permissions'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',

        ];

        Validator::make($request->all(), [
            'name' => 'required',

            'email' => 'required|unique:users',
            'password' => 'required',
            'enable' => 'required',

        ], $messages)->validate();
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->enable = $request->enable;
        $user->save();
        if ($user->save()) {
            $user->permission()->attach($request->title);

        }
        return redirect('user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->can('edit', User::class)) {
            $permissions = Permission::all();

            $user = User::find($id);
            $user_permissions = $user->permission;
            $user_permission_ids = $user_permissions->pluck('id')->toArray();
            if ($user_permissions->count()) {
                foreach ($permissions as $user_permission) {
                    if (in_array($user_permission->id, $user_permission_ids)) {
                        $user_permission->status = true;

                    } else {
                        $user_permission->status = false;

                    }

                }
                return view('user/edit', compact('user', 'id', 'permissions'));

            }

            return view('user/edit', compact('user', 'id', 'permissions'));

        } else {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find((int) request()->segment(2));
        $messages = [
            'required' => 'This field is required.',
            'unique' => 'This field must be unique.',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:companies,email,' . $user->id,
        ], $messages);
        if ($request->password && $request->confirm_password == $request->password) {
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();
            Alert::alert('Updated', 'Your profile is updated & please relogin.', 'success');
            return redirect('login');

        } else {
            $user->name = $request->name;
            $user->save();
            Alert::alert('Updated', 'Your profile is updated', 'success');
            return redirect('profile');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->can('delete', User::class)) {
            $user = User::find($id);
            $user_permission = $user->permission;

            if ($user_permission->count()) {
                $user = User::destroy($id);
                if ($user) {
                    DB::table("permission_user")->where('user_id', $id)->delete();

                }

            } else {
                $user = User::destroy($id);

            }
            return redirect('user')
                ->with('success', 'User deleted successfully');

        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('user.profile');
    }

    public function updateProfilePic(Request $request)
    {
        $image = time() . rand(1, 100) . '.' . explode('/', explode(':', substr($request->image, 0, strpos($request->image, ';')))[1])[1];
        $path = "app/public/profile/";
        $location = storage_path($path . $image);
        Image::make($request->image)->save($location);

        $user = Auth::user();
        $del_image_location = storage_path("app/public/profile/" . $user->image);
        if (File::exists($del_image_location)) {
            File::delete($del_image_location);
        }

        $user->image = $image;
        $user->save();

    }
    public function search(Request $request)
    {
        $search = \Request::get('search');
        if ($search) {
            $users = User::where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%");
            })->paginate(10);
        } else {
            $users = User::latest()->paginate(10);
        }
        return view('user.index', ['users' => $users, 'search' => $search]);

    }
}
