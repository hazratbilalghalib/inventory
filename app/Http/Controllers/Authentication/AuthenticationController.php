<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AuthenticationController extends Controller
{
 public function login(Request $request)
 {
  $messages = [
   'required' => 'This field is required.',
   'email'    => 'This field must be a valid email.'
  ];

  $validator = Validator::make($request->all(), [
   'email'    => 'required|email',
   'password' => 'required'
  ], $messages);

  $credientials = array(
   'email'    => $request->email,
   'password' => $request->password,
  );

  $remember_me = $request->has('remember') ? true : false;

  if ($validator->fails()) {
   return Redirect::to('login')
    ->withErrors($validator)
    ->withInput();
  } else {
   if (Auth::attempt($credientials, $remember_me)) {
    return redirect::to('/home');
   } else {
    toastr()->warning('Incorrect Email and Password!');
    return redirect::to('login')->withInput();
   }
  }

 }
 /**
  * Create token password reset
  *
  * @param  [string] email
  * @return [string] message
  */
 public function resetRequest(Request $request)
 {
  $request->validate([
   'email' => 'required|string|email',
  ]);

  $user = User::where('email', $request->email)->first();
  if (!$user) {
   toastr()->warning('We cant find a user with that e-mail address.');
   return redirect::to('login')->withInput();
  }

  $passwordReset = PasswordReset::updateOrCreate(
   ['email' => $user->email],
   [
    'email' => $user->email,
    'token' => \Str::random(60),
   ]
  );
  if ($user && $passwordReset) {
   $user->notify(
    new PasswordResetRequest($passwordReset->token)
   );
  }
  toastr()->warning('We have e-mailed your password reset link!');
  return redirect::to('login')->withInput();
 }
 /**
  * Find token password reset
  *
  * @param  [string] $token
  * @return [string] message
  * @return [json] passwordReset object
  */
 public function find($token)
 {
  $passwordReset = PasswordReset::where('token', $token)
   ->first();
  if (!$passwordReset) {
   toastr()->warning('This password reset token is invalid.');
   return redirect::to('login');
  }

  if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
   $passwordReset->delete();
   toastr()->warning('This password reset token is expired.');
   return redirect::to('login');
  }
  $user = User::where('email', $passwordReset->email)->first();
  return view('auth/reset_password', compact('passwordReset', 'user'));
 }
 /**
  * Reset password
  *
  * @param  [string] email
  * @param  [string] password
  * @param  [string] password_confirmation
  * @param  [string] token
  * @return [string] message
  * @return [json] user object
  */
 public function reset(Request $request)
 {
  $messages = [
   'required' => 'This field is required.',
  ];

  $request->validate([
   'email'    => 'required|string|email',
   'password' => 'required|string|confirmed',
   'token'    => 'required|string',
  ]);

  $passwordReset = PasswordReset::where([
   ['token', $request->token],
   ['email', $request->email],
  ])->first();
  if (!$passwordReset) {
   toastr()->warning('This password reset token is invalid or expired.');
   return redirect::to('login');
  }

  $user = User::where('email', $passwordReset->email)->first();
  if (!$user) {
   toastr()->warning('We cant find a user with that e-mail address.');
   return redirect::to('login');
  }

  $user->password = bcrypt($request->password);
  $user->save();
  $passwordReset->delete();
  $user->notify(new PasswordResetSuccess($passwordReset));
  toastr()->warning('Please login with your new password!');
  return redirect::to('login');
 }

 public function logout(Request $request)
 {
  Auth::logout();
  return redirect('login');
 }
}
