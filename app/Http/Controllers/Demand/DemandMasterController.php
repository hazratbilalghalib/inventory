<?php

namespace App\Http\Controllers\Demand;

use App\Consumer;
use App\DemandDetail;
use App\DemandMaster;
use App\Http\Controllers\Controller;
use App\Item;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DemandMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->can('view', DemandMaster::class)) {
            $demands = DemandMaster::with('consumer')->orderBy('demand_date', 'desc')->paginate(10);
            return view('demand.index')->with(['demands' => $demands]);
        } else {
            return redirect("/home");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', DemandMaster::class)) {
            $dispatchers = Consumer::where('type', 'dispatcher')->get();

            $lastDemandNo = DemandMaster::OrderBy('created_at', 'DESC')->first();
            if (!empty($lastDemandNo)) {
                $demand_no = $lastDemandNo->demand_no;
                ++$demand_no;
            } else {
                $demand_no = 11;
            }
            return view('demand.create')->with(['demandNo' => $demand_no, 'dispatchers' => $dispatchers]);
        } else {
            return redirect("/home");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'required' => 'This field is required.',
        ];

        Validator::make($request->all(), [
            'consumer_id' => 'required',
            'demand_no' => 'required',
            'demand_date' => 'required',
            'shift' => 'required',
        ], $messages)->validate();
        $Item_Ids_Arr = array_filter($request->item_id, function ($value) {
            return !is_null($value);
        });

        $Item_Codes_Arr = array_filter($request->item_code, function ($value) {
            return !is_null($value);
        });

        $Item_Names_Arr = array_filter($request->item_name, function ($value) {
            return !is_null($value);
        });

        $Item_Quantities_Arr = array_filter($request->item_quantity, function ($value) {
            return !is_null($value);
        });

        $Demand_type_Arr = array_filter($request->demand_type, function ($value) {
            return !is_null($value);
        });

        $DemandMaster = DemandMaster::Create(
            [

                "demand_date" => $request->demand_date,
                "consumer_id" => $request->consumer_id,
                "demand_no" => $request->demand_no,
                "remarks" => $request->remark,
                "shift" => $request->shift,
                "person_name" => $request->person_name,

            ]
        );

        $length = count($Item_Quantities_Arr);
        for ($i = 0; $i < $length; $i++) {

            $DemandDetail = DemandDetail::create(
                [
                    "demand_master_id" => $DemandMaster->id,
                    "item_id" => $Item_Ids_Arr[$i],
                    "quantity" => $Item_Quantities_Arr[$i],
                    "demand_type" => $Demand_type_Arr[$i],

                ]
            );

        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->can('edit', DemandMaster::class)) {
            $dispatchers = Consumer::where('type', 'dispatcher')->get();

            $demandMaster = DemandMaster::with('consumer')->where('id', $id)->first();

            $demandDetails = DemandDetail::with('item.itemCategory')->where('demand_master_id', $demandMaster->id)->get();

            return view('demand.edit')->with(['demandMaster' => $demandMaster, 'demandDetails' => $demandDetails, 'dispatchers' => $dispatchers, 'id' => $id]);
        } else {
            return redirect("/home");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->can('update', DemandMaster::class)) {
            $messages = [
                'required' => 'This field is required.',
            ];

            Validator::make($request->all(), [
                'consumer_id' => 'required',
                'demand_no' => 'required',
                'demand_date' => 'required',
                'shift' => 'required',
            ], $messages)->validate();

            $Item_Ids_Arr = array_filter($request->item_id, function ($value) {
                return !is_null($value);
            });
            $Ids_Arr = array_filter($request->id, function ($value) {
                return !is_null($value);
            });

            $Item_Codes_Arr = array_filter($request->item_code, function ($value) {
                return !is_null($value);
            });

            $Item_Names_Arr = array_filter($request->item_name, function ($value) {
                return !is_null($value);
            });
            $Item_Quantities_Arr = array_filter($request->item_quantity, function ($value) {
                return !is_null($value);
            });

            $Demand_type_Arr = array_filter($request->demand_type, function ($value) {
                return !is_null($value);
            });

            $demandMaster = DemandMaster::Where('id', $id)->update(
                [

                    "demand_date" => $request->demand_date,
                    "consumer_id" => $request->consumer_id,
                    "demand_no" => $request->demand_no,
                    "remarks" => $request->remark,
                    "shift" => $request->shift,
                    "person_name" => $request->person_name,
                ]
            );

            $length = count($Item_Quantities_Arr);
            for ($i = 0; $i < $length; $i++) {
                if ($Ids_Arr[$i] != 0) {
                    $demandDetail = DemandDetail::where(["demand_master_id" => $id, 'id' => $Ids_Arr[$i]])->update(
                        [

                            "item_id" => $Item_Ids_Arr[$i],
                            "quantity" => $Item_Quantities_Arr[$i],
                            "demand_type" => $Demand_type_Arr[$i],

                        ]
                    );
                } else {
                    $demandDetail = DemandDetail::create(
                        [
                            "demand_master_id" => $id,
                            "item_id" => $Item_Ids_Arr[$i],
                            "quantity" => $Item_Quantities_Arr[$i],
                            "demand_type" => $Demand_type_Arr[$i],

                        ]
                    );
                }
            }
            return redirect()->back();
        } else {
            return redirect("/home");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->can('delete', DemandMaster::class)) {
            DemandMaster::destroy($id);
            \Alert::alert('Delete', 'Your Record is deleted!', 'success');
            return redirect('demand');
        } else {
            return redirect("/home");
        }
    }

    public function search()
    {
        $search = \Request::get('search');
        if ($search) {
            $demands = DemandMaster::with('consumer')->

                orderBy('demand_date', 'desc')
                ->where(function ($query) use ($search) {
                    $query->where('shift', 'LIKE', "%$search%")
                        ->orWhere('demand_no', 'LIKE', "%$search%")
                        ->orWhere('demand_date', 'LIKE', "%$search%")

                        ->orWhereHas('consumer', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%");
                        });

                })->paginate(10);
        } else {
            $demands = DemandMaster::with('consumer')->orderBy('demand_date', 'desc')->paginate(10);

        }
        return view('demand.index')->with(['demands' => $demands]);
    }

    public function itemSearch(Request $request)
    {

        $search = \Request::get('item_name');

        if ($search) {

            $items = Item::With('itemCategory')->

                where(function ($query) use ($search) {
                $query->where('name', "$search")
                    ->orWhere('item_code', "$search")
                    ->orWhere('barcode_txt', "$search")
                    ->orWhereHas('itemCategory', function ($query) use ($search) {
                        $query->where('name', "$search");
                    });

            })->limit(300)->get();

            if ($items->count()) {

                $length = $items->count();
                return response()->json(['items' => $items, 'length' => $length], 200);
            } else {
                $items = Item::With('itemCategory')->

                    where(function ($query) use ($search) {
                    $query->where('name', "LIKE", "%$search%")
                        ->orWhere('item_code', "LIKE", "%$search%")
                        ->orWhere('barcode_txt', "LIKE", "%$search%")
                        ->orWhereHas('itemCategory', function ($query) use ($search) {
                            $query->where('name', "LIKE", "%$search%");

                        });

                })->limit(300)->get();

                $length = $items->count();
                return response()->json(['items' => $items, 'length' => $length], 200);

            }

        }

    }

    public function itemSearchList(Request $request)
    {

        $search = \Request::get('item_name');

        if ($search) {

            $items = Item::With('itemCategory')

                ->where(function ($query) use ($search) {
                    $query->where('name', "LIKE", "%$search%")
                        ->orWhere('item_code', "LIKE", "%$search%")
                        ->orWhere('barcode_txt', "LIKE", "%$search%")
                        ->orWhereHas('itemCategory', function ($query) use ($search) {
                            $query->where('name', "LIKE", "%$search%");

                        });

                })->limit(300)->get();
            if ($items->count()) {

                $length = $items->count();
                return response()->json(['items' => $items, 'length' => $length], 200);
            } else {
                $items = Item::With('itemCategory')->limit(300)->get();

                $length = $items->count();
                return response()->json(['items' => $items, 'length' => $length], 200);
            }

        }

    }
}
