<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_master_id', 'item_id', 'item_quantity', 'item_rate', 'item_discount_percentage', 'total_amount'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function transactionMaster()
    {
        return $this->belongsTo('App\TransactionMaster');
    }

    
}
