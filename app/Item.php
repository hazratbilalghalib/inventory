<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'items';

 /**
  * The database primary key value.
  *
  * @var string
  */
 protected $primaryKey = 'id';

 /**
  * Attributes that should be mass-assignable.
  *
  * @var array
  */
 protected $fillable = ['name', 'item_category_id', 'min_stock_level', 'max_stock_level', 'avg_purchase_rte','avg_sale_rte', 'item_type', 'brand_name', 'unit_abbrev', 'size_title', 'color_name',
'quality_type', 'barcode_txt', 'barcode_ind', 'item_code', 'item_urdu','margin_one','margin_two'];

 public function itemCategory()
 {
  return $this->belongsTo('App\ItemCategory');
 }

 public function transactionDetails()
 {
  return $this->hasMany('App\TransactionDetail');
 }

 public function demandDetails()
 {
  return $this->hasMany('App\DemandDetail');
 }


}
