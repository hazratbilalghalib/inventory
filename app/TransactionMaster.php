<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionMaster extends Model
{
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'transaction_masters';

 /**
  * The database primary key value.
  *
  * @var string
  */
 protected $primaryKey = 'id';

 /**
  * Attributes that should be mass-assignable.
  *
  * @var array
  */
 protected $fillable = ['consumer_id', 'type', 'receipt_no', 'transaction_date', 'driver_name', 'van_no', 'supplier_ref_no', 'serial_no', 'remark', 'return_ind'];

 public function consumer()
 {
  return $this->belongsTo('App\Consumer');
 }

 public function transactionDetails()
 {
  return $this->hasMany('App\TransactionDetail');
 }

 
}
