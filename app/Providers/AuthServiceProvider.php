<?php

namespace App\Providers;
use DB;



use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        // Consumer::class => ConsumerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        
    //     $gate->define('consumer',function($user){
    //    $permission_id = DB::table('permissions')->where('title','consumer')->first()->id;
    //    $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
    //    if($user_permission->count())

    //    {
    //        return true;
    //    }


    //     });
    //     $gate->define('edit-consumer',function($user){
    //         $permission_id = DB::table('permissions')->where('title','edit-consumer')->first()->id;
    //         $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
    //         if($user_permission->count())
     
    //         {
    //             return true;
    //         }
     
     
    //          });
    //          $gate->define('add-consumer',function($user){
    //             $permission_id = DB::table('permissions')->where('title','add-consumer')->first()->id;
    //             $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
    //             if($user_permission->count())
         
    //             {
    //                 return true;
    //             }
         
         
                //  });

    }
}
