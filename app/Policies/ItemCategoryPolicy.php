<?php

namespace App\Policies;

use App\ItemCategory;
use App\User;
use DB;

use Illuminate\Auth\Access\HandlesAuthorization;

class ItemCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any item categories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the item category.
     *
     * @param  \App\User  $user
     * @param  \App\ItemCategory  $itemCategory
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','view-item-category')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;

        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
        else{
            return false;
        }
    }

    /**
     * Determine whether the user can create item categories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','add-item-category')->first()->id;
            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
     
    }

    /**
     * Determine whether the user can update the item category.
     *
     * @param  \App\User  $user
     * @param  \App\ItemCategory  $itemCategory
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','update-item-category')->first()->id;
            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
    }

    /**
     * Determine whether the user can delete the item category.
     *
     * @param  \App\User  $user
     * @param  \App\ItemCategory  $itemCategory
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','delete-item-category')->first()->id;
            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
    }

    /**
     * Determine whether the user can restore the item category.
     *
     * @param  \App\User  $user
     * @param  \App\ItemCategory  $itemCategory
     * @return mixed
     */
    public function restore(User $user, ItemCategory $itemCategory)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the item category.
     *
     * @param  \App\User  $user
     * @param  \App\ItemCategory  $itemCategory
     * @return mixed
     */
    public function forceDelete(User $user, ItemCategory $itemCategory)
    {
        //
    }
    public function edit(User $user){
        $permission_id = DB::table('permissions')->where('title','edit-item-category')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
}
