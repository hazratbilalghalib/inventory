<?php

namespace App\Policies;

use App\DemandMaster;
use App\User;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class DemandMasterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any demand masters.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the demand master.
     *
     * @param  \App\User  $user
     * @param  \App\DemandMaster  $demandMaster
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'view-demand')->first();

        if (!empty($permission_id)) {
            $permission_id = $permission_id->id;
            $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();

            if ($user_permission->count()) {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create demand masters.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

        $permission_id = DB::table('permissions')->where('title', 'add-demand')->first();
        if (!empty($permission_id)) {
            $permission_id = $permission_id->id;
            $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();

            if ($user_permission->count()) {
                return true;
            }
        } else {
            return false;
        }

    }

    /**
     * Determine whether the user can update the demand master.
     *
     * @param  \App\User  $user
     * @param  \App\DemandMaster  $demandMaster
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'update-demand')->first();
        if (!empty($permission_id)) {
            $permission_id = $permission_id->id;

            $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
            if ($user_permission->count()) {
                return true;
            }
        } else {
            return false;
        }

    }

    /**
     * Determine whether the user can delete the demand master.
     *
     * @param  \App\User  $user
     * @param  \App\DemandMaster  $demandMaster
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'delete-demand')->first();
        if (!empty($permission_id)) {
            $permission_id = $permission_id->id;
            $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
            if ($user_permission->count()) {
                return true;
            }
        } else {
            return false;
        }

    }

    /**
     * Determine whether the user can restore the demand master.
     *
     * @param  \App\User  $user
     * @param  \App\DemandMaster  $demandMaster
     * @return mixed
     */
    public function restore(User $user, DemandMaster $demandMaster)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the demand master.
     *
     * @param  \App\User  $user
     * @param  \App\DemandMaster  $demandMaster
     * @return mixed
     */
    public function forceDelete(User $user, DemandMaster $demandMaster)
    {
        //
    }
    public function edit(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'edit-demand')->first();
        if (!empty($permission_id)) {
            $permission_id = $permission_id->id;
            $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
            if ($user_permission->count()) {
                return true;
            }
        } else {
            return false;
        }
    }
}
