<?php

namespace App\Policies;

use App\TransactionMaster;
use App\User;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionMasterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any transaction masters.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the transaction master.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionMaster  $transactionMaster
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'view-transaction')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can create transaction masters.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'add-transaction')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }

    /**
     * Determine whether the user can update the transaction master.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionMaster  $transactionMaster
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'update-transaction')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }

    /**
     * Determine whether the user can delete the transaction master.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionMaster  $transactionMaster
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'delete-transaction')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }

    /**
     * Determine whether the user can restore the transaction master.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionMaster  $transactionMaster
     * @return mixed
     */
    public function restore(User $user, TransactionMaster $transactionMaster)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the transaction master.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionMaster  $transactionMaster
     * @return mixed
     */
    public function forceDelete(User $user, TransactionMaster $transactionMaster)
    {
        //
    }
    public function edit(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'edit-transaction')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }
}
