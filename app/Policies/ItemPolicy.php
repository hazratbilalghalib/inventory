<?php

namespace App\Policies;

use App\Item;
use App\User;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any items.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'view-item')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can create items.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'add-item')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'update-item')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        dd($user_permission);
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'delete-item')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function restore(User $user, Item $item)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function forceDelete(User $user, Item $item)
    {
        //
    }
    public function edit(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'edit-item')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }
}
