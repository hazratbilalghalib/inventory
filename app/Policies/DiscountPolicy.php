<?php

namespace App\Policies;
use DB;
use App\Discount;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiscountPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any discounts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the discount.
     *
     * @param  \App\User  $user
     * @param  \App\Discount  $discount
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','view-discount')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;

                
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
        else{
            return false;
        }
    }

    /**
     * Determine whether the user can create discounts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','add-discount')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
    else{
        return false;
    }

    }

    /**
     * Determine whether the user can update the discount.
     *
     * @param  \App\User  $user
     * @param  \App\Discount  $discount
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','update-discount')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the discount.
     *
     * @param  \App\User  $user
     * @param  \App\Discount  $discount
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','delete-discount')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the discount.
     *
     * @param  \App\User  $user
     * @param  \App\Discount  $discount
     * @return mixed
     */
    public function restore(User $user, Discount $discount)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the discount.
     *
     * @param  \App\User  $user
     * @param  \App\Discount  $discount
     * @return mixed
     */
    public function forceDelete(User $user, Discount $discount)
    {
        //
    }
    public function edit(User $user){
        $permission_id = DB::table('permissions')->where('title','edit-discount')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
}
