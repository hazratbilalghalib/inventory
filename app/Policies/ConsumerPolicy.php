<?php

namespace App\Policies;

use App\Consumer;
use App\User;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConsumerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any consumers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the consumer.
     *
     * @param  \App\User  $user
     * @param  \App\Consumer  $consumer
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','view-consumer')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        
        if($user_permission->count())
 
        {
            return true;
        }
    }
    else{
        return false;
    }
    }

    /**
     * Determine whether the user can create consumers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
            $permission_id = DB::table('permissions')->where('title','add-consumer')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;

            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
            else{
                return false;
            }
        
        }
     
     
    }

    /**
     * Determine whether the user can update the consumer.
     *
     * @param  \App\User  $user
     * @param  \App\Consumer  $consumer
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','update-consumer')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;

            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
        }
        else{
            return false;
        }

    }

    /**
     * Determine whether the user can delete the consumer.
     *
     * @param  \App\User  $user
     * @param  \App\Consumer  $consumer
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title','delete-consumer')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;
            $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
            if($user_permission->count())
     
            {
                return true;
            }
        }
        else{
            return false;
        }

    }

    /**
     * Determine whether the user can restore the consumer.
     *
     * @param  \App\User  $user
     * @param  \App\Consumer  $consumer
     * @return mixed
     */
    public function restore(User $user, Consumer $consumer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the consumer.
     *
     * @param  \App\User  $user
     * @param  \App\Consumer  $consumer
     * @return mixed
     */
    public function forceDelete(User $user, Consumer $consumer)
    {
        //
    }
    public function edit(User $user){
        $permission_id = DB::table('permissions')->where('title','edit-consumer')->first();
        if(!empty(  $permission_id))
        {
            $permission_id =    $permission_id->id;
        $user_permission = DB::table('permission_user')->where(['user_id'=>$user->id , 'permission_id'=>$permission_id])->get();
        if($user_permission->count())
 
        {
            return true;
        }
    }
    else{
        return false;
    }

    }
}
