<?php

namespace App\Policies;

use App\TransactionDetail;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionDetailPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any transaction details.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the transaction detail.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionDetail  $transactionDetail
     * @return mixed
     */
    public function view(User $user, TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Determine whether the user can create transaction details.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the transaction detail.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionDetail  $transactionDetail
     * @return mixed
     */
    public function update(User $user, TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Determine whether the user can delete the transaction detail.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionDetail  $transactionDetail
     * @return mixed
     */
    public function delete(User $user, TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Determine whether the user can restore the transaction detail.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionDetail  $transactionDetail
     * @return mixed
     */
    public function restore(User $user, TransactionDetail $transactionDetail)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the transaction detail.
     *
     * @param  \App\User  $user
     * @param  \App\TransactionDetail  $transactionDetail
     * @return mixed
     */
    public function forceDelete(User $user, TransactionDetail $transactionDetail)
    {
        //
    }
}
