<?php

namespace App\Policies;

use App\User;
use DB;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'view-user')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

        $permission_id = DB::table('permissions')->where('title', 'add-user')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'update-user')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;

        }

    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'delete-user')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }
    public function edit(User $user)
    {
        $permission_id = DB::table('permissions')->where('title', 'edit-user')->first()->id;
        $user_permission = DB::table('permission_user')->where(['user_id' => $user->id, 'permission_id' => $permission_id])->get();
        if ($user_permission->count()) {
            return true;
        }

    }
}
