<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemandDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'demand_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['demand_master_id', 'item_id','demand_type','quantity'];


    public function demandMaster()
    {
        return $this->belongsTo("App\DemandMaster");

    }

    public function item()
    {
        return $this->belongsTo("App\Item");
        
    }


    
}
