<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{

 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'consumers';

 /**
  * The database primary key value.
  *
  * @var string
  */
 protected $primaryKey = 'id';

 /**
  * Attributes that should be mass-assignable.
  *
  * @var array
  */
 protected $fillable = ['name', 'type','order_limit', 'sales_representative', 'region', 'sales_tax_registration_no', 'person_to_contact', 'designation', 'country', 'province', 'city', 'address_one', 'address_two', 'zip', 'phone', 'fax', 'email', 'bank_details', 'remarks'];

 public function transactionMasters()
 {
  return $this->hasMany('App\TransactionMaster');
 }

 public function demandMasters()
 {
  return $this->hasMany('App\DemandMaster');
 }

 

}
