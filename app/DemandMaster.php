<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemandMaster extends Model
{
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'demand_masters';

 /**
  * The database primary key value.
  *
  * @var string
  */
 protected $primaryKey = 'id';

 /**
  * Attributes that should be mass-assignable.
  *
  * @var array
  */
 protected $fillable = ['consumer_id', 'demand_date','shift','demand_no', 'person_name', 'remarks'];

 public function consumer()
 {
  return $this->belongsTo('App\Consumer');
 }

 public function demandDetails()
 {
  return $this->hasMany('App\DemandDetail');
 }

 

}
